#!/usr/bin/env lua
---------------------------------------------------------------------
--     This Lua5 script is Copyright (c) 2020, Peter J Billam      --
--                         pjb.com.au                              --
--  This script is free software; you can redistribute it and/or   --
--         modify it under the same terms as Lua5 itself.          --
---------------------------------------------------------------------
local Version = '1.0  for Lua5'
local VersionDate  = '12apr2020'
local Synopsis = [[
# defaults are pulse=262 mS, 15 minutes, with the piano-pulse, seed varies
  lua in_c.lua | aplaymidi -
  in_c.lua -p 280 > /tmp/t.mid  # set the Pulse in milliseconds (default 262)
  in_c.lua -m 20  > /tmp/t.mid  # set the approximate duration in Minutes
  in_c.lua -q > /tmp/t.mid      # Quietens the optional piano-pulse
  in_c.lua -s 975 > /tmp/t.mid  # Seeds the random-number-generator
  in_c.lua -h                   # read this Help
  perldoc in_c.lua              # read the manual :-)
]]

MIDI = require 'MIDI'

-- These can be reset
Pulse   = 262       -- the pulse, in milliseconds
Minutes = 15        -- the approximate overall duration
Quiet   = false     -- suppresses the pulse on the piano
Seed    = nil       -- seeds the random-number-generator

------- from ~/lua/pjblib.lua -------
function warn(...)
    local a = {}
    for k,v in pairs{...} do table.insert(a, tostring(v)) end
    io.stderr:write(table.concat(a),'\n') ; io.stderr:flush()
end
function die(...) warn(...);  os.exit(1) end
function round(x)
    if not x then return nil end
    return math.floor(x+0.5)
end
------- from randomdist.lua
function rayleigh_irand(average)
	return round(average * math.sqrt( -2 * math.log(1-math.random()) ))
end
------- from midigenerators.lua
my_score = {
    1000,  -- ticks per beat
    {    -- first track
        {'set_tempo', 5, 1000000},
    },  -- end of first track
}
function add_event(event)
    if not type(event) == 'table' then
      die('add_event: arg must be a table, not a ',type(event))
    end
    table.insert(my_score[2], event)
end
function write_score()
    io.stdout:write(MIDI.score2midi(my_score))
end


local iarg=1; while arg[iarg] ~= nil do
	if not string.find(arg[iarg], '^-[a-z]') then break end
	local first_letter = string.sub(arg[iarg],2,2)
	if first_letter == 'v' then
		local n = string.gsub(arg[0],"^.*/","",1)
		print(n.." version "..Version.."  "..VersionDate)
		os.exit(0)
	elseif first_letter == 'm' then
		iarg    = iarg+1
		Minutes = tonumber(arg[iarg])
	elseif first_letter == 'p' then
		iarg  = iarg+1
		Pulse = tonumber(arg[iarg])
	elseif first_letter == 'q' then
		Quiet = true
	elseif first_letter == 's' then
		iarg  = iarg+1
		Seed  = tonumber(arg[iarg])
	else
		local n = string.gsub(arg[0],"^.*/","",1)
		print(n.." version "..Version.."  "..VersionDate.."\n\n"..Synopsis)
		os.exit(0)
	end
	iarg = iarg+1
end

if Seed then
	math.randomseed(Seed)
else
	math.randomseed(os.time())
end

dt8    = Pulse
dt8dot = dt8 * 3 / 2
dt4    = dt8 * 2
dt4dot = dt8 * 3
dt2    = dt8 * 4
dt2dot = dt8 * 6
dt1    = dt8 * 8
dt1dot = dt8 * 12
dt16   = dt8 / 2
grace  = (66 * dt8/5) ^ 0.5
Finished = 1000

-- the values 4,6,20,20 give 826, 831, 834, 845, 851 sec,  av = 837
-- so these hard-wired 4,6,20,20 now get scaled from Minutes :
DelayFix  = round( 4 * Minutes*60/837)
DelayRand = round( 6 * Minutes*60/837)
DuratFix  = round(20 * Minutes*60/837)  -- doesn't actually need to be Pulses
DuratRand = round(20 * Minutes*60/837)  -- doesn't actually need to be Pulses

AveragePulsesPerPhrase = round(Minutes * 60*1000 / (53 * Pulse))
-- No ... this rounding to the Pulse needs to happen in the loop ...

Instruments = {  -- { patch, volume, is_up_an_octave }
	[0]  = {  0,  55 },  -- the pulse
	[1]  = { 13,  80 },  -- xylophone
	[2]  = { 71,  75, true},  -- clarinet
	[3]  = { 12,  95 },  -- marimba
	[4]  = {107,  80 },  -- koto
	[5]  = { 75,  85 },  -- pan-pipes
	[6]  = { 15,  80 },  -- dulcimer
	[7]  = { 64,  80 },  -- soprano sax
	[8]  = { 10,  80 },  -- music box
	[10] = { 75,  75, true },  -- pan-pipes
	[11] = { 11,  80 },  -- vibraphone
	[12] = { 17,  60 },  -- percussive organ
	[13] = { 46,  80 },  -- harp
	[14] = { 24,  85 },  -- acoustic guitar nylon-string
	[15] = { 74,  80, true},  -- recorder
}

-- should be able to transpose some instruments up an octave ...
function add_note (time, duration, channel, pitch , vol)
	local instrument = Instruments[channel]
	if instrument[3] then pitch = pitch + 12 end
	add_event({'note', time, duration, channel, pitch, vol})
end

Phrases = {
	[1] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time-grace, grace, instrumentnum, 60, vol)
			add_note(time,   dt4-grace, instrumentnum, 64, vol)
			time = time + dt4
			add_note(time-grace, grace, instrumentnum, 60, vol)
			add_note(time,   dt4-grace, instrumentnum, 64, vol)
			time = time + dt4
			add_note(time-grace, grace, instrumentnum, 60, vol)
			add_note(time,   dt4-grace, instrumentnum, 64, vol)
			time = time + dt4
		end
	end ,
	[2] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time-grace, grace, instrumentnum, 60, vol)
			add_note(time,   dt8, instrumentnum, 64, vol)
			time = time + dt8
			add_note(time,   dt8, instrumentnum, 65, vol)
			time = time + dt8
			add_note(time,   dt4, instrumentnum, 64, vol)
			time = time + dt4
		end
	end ,
	[3] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			time = time + dt8
			add_note(time,   dt8, instrumentnum, 64, vol)
			time = time + dt8
			add_note(time,   dt8, instrumentnum, 65, vol)
			time = time + dt8
			add_note(time,   dt8, instrumentnum, 64, vol)
			time = time + dt8
		end
	end ,
	[4] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			time = time + dt8
			add_note(time,   dt8, instrumentnum, 64, vol)
			time = time + dt8
			add_note(time,   dt8, instrumentnum, 65, vol)
			time = time + dt8
			add_note(time,   dt8, instrumentnum, 67, vol)
			time = time + dt8
		end
	end ,
	[5] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt8, instrumentnum, 64, vol)
			time = time + dt8
			add_note(time,   dt8, instrumentnum, 65, vol)
			time = time + dt8
			add_note(time,   dt8, instrumentnum, 67, vol)
			time = time + dt4
		end
	end ,
	[6] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt1*1.8, instrumentnum, 72, vol)
			time = time + dt1*2
		end
	end ,
	[7] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			time = time + dt8*7
			add_note(time,   dt16, instrumentnum, 60, vol+15)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 60, vol+15)
			time = time + dt16
			add_note(time,   dt8 , instrumentnum, 60, vol+15)
			time = time + dt4*5
		end
	end ,
	[8] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt1dot, instrumentnum, 67, vol)
			time = time + dt1dot
			add_note(time,   dt1*1.8, instrumentnum, 65, vol)
			time = time + dt1*2
		end
	end ,
	[9] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 71, vol+10)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol+10)
			time = time + dt16 + dt4*3
		end
	end ,
	[10] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 71, vol-5)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol-5)
			time = time + dt16
		end
	end ,
	[11] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 65, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
		end
	end ,
	[12] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt8, instrumentnum, 65, vol)
			time = time + dt8
			add_note(time,   dt8, instrumentnum, 67, vol)
			time = time + dt8
			add_note(time,   dt1, instrumentnum, 71, vol)
			time = time + dt1
			add_note(time,   dt4, instrumentnum, 72, vol)
			time = time + dt4
		end
	end ,
	[13] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,  dt16,   instrumentnum, 71, vol+10)
			time = time + dt16
			add_note(time,  dt8dot, instrumentnum, 67, vol+10)
			time = time + dt8dot
			add_note(time,  dt16,   instrumentnum, 67, vol+10)
			time = time + dt16
			add_note(time,  dt16,   instrumentnum, 65, vol+10)
			time = time + dt16
			add_note(time,  dt8,    instrumentnum, 67, vol+10)
			time = time + dt8 + dt8dot
			add_note(time,  dt2dot, instrumentnum, 67, vol+10)
			time = time + dt16 + dt2dot
		end
	end ,
	[14] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt1, instrumentnum, 72, vol)
			time = time + dt1
			add_note(time,   dt1, instrumentnum, 71, vol)
			time = time + dt1
			add_note(time,   dt1, instrumentnum, 67, vol)
			time = time + dt1
			add_note(time,   dt1, instrumentnum, 66, vol)
			time = time + dt1
		end
	end ,
	[15] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt1
		end
	end ,
	[16] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 72, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
		end
	end ,
	[17] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 72, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 72, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt8
		end
	end ,
	[18] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 66, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 66, vol)
			time = time + dt16
			add_note(time,   dt8,  instrumentnum, 64, vol)
			time = time + dt8dot
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
		end
	end ,
	[19] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			time = time + dt4dot
			add_note(time,   dt4dot, instrumentnum, 77, vol)
			time = time + dt4dot
		end
	end ,
	[20] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 66, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 66, vol)
			time = time + dt16
			add_note(time, dt8dot, instrumentnum, 55, vol)
			time = time + dt8dot
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 66, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 66, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
		end
	end ,
	[21] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt2+dt8, instrumentnum, 66, vol)
			time = time + dt2dot
		end
	end ,
	[22] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			for i = 1,5 do
				add_note(time, dt4+dt16, instrumentnum, 64, vol)
				time = time + dt4dot
			end
			add_note(time, dt4+dt16, instrumentnum, 66, vol)
			time = time + dt4dot
			add_note(time, dt4+dt16, instrumentnum, 67, vol)
			time = time + dt4dot
			add_note(time, dt4+dt16, instrumentnum, 69, vol)
			time = time + dt4dot
			add_note(time, dt8,    instrumentnum, 71, vol)
			time = time + dt8
		end
	end ,
	[23] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time, dt8,    instrumentnum, 64, vol)
			time = time + dt8
			for i = 1,5 do
				add_note(time, dt4+dt16, instrumentnum, 66, vol)
				time = time + dt4dot
			end
			add_note(time, dt4+dt16, instrumentnum, 67, vol)
			time = time + dt4dot
			add_note(time, dt4+dt16, instrumentnum, 69, vol)
			time = time + dt4dot
			add_note(time, dt4,    instrumentnum, 71, vol)
			time = time + dt4
		end
	end ,
	[24] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time, dt8,    instrumentnum, 64, vol)
			time = time + dt8
			add_note(time, dt8,    instrumentnum, 66, vol)
			time = time + dt8
			for i = 1,5 do
				add_note(time, dt4+dt16, instrumentnum, 67, vol)
				time = time + dt4dot
			end
			add_note(time, dt4+dt16, instrumentnum, 69, vol)
			time = time + dt4dot
			add_note(time, dt8,    instrumentnum, 71, vol)
			time = time + dt8
		end
	end ,
	[25] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time, dt8,    instrumentnum, 64, vol)
			time = time + dt8
			add_note(time, dt8,    instrumentnum, 66, vol)
			time = time + dt8
			add_note(time, dt8,    instrumentnum, 67, vol)
			time = time + dt8
			for i = 1,5 do
				add_note(time, dt4+dt16, instrumentnum, 69, vol)
				time = time + dt4dot
			end
			add_note(time, dt4+dt16,    instrumentnum, 71, vol)
			time = time + dt4dot
		end
	end ,
	[26] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time, dt8,    instrumentnum, 64, vol)
			time = time + dt8
			add_note(time, dt8,    instrumentnum, 66, vol)
			time = time + dt8
			add_note(time, dt8,    instrumentnum, 67, vol)
			time = time + dt8
			add_note(time, dt8,    instrumentnum, 69, vol)
			time = time + dt8
			for i = 1,5 do
				add_note(time, dt4+dt16, instrumentnum, 71, vol)
				time = time + dt4dot
			end
		end
	end ,
	[27] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 66, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 66, vol)
			time = time + dt16
			add_note(time,   dt8,  instrumentnum, 67, vol)
			time = time + dt8
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 66, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 66, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
		end
	end ,
	[28] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 66, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 66, vol)
			time = time + dt16
			add_note(time,   dt8,  instrumentnum, 64, vol)
			time = time + dt8dot
			add_note(time,   dt16, instrumentnum, 64, vol)
			time = time + dt16
		end
	end ,
	[29] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time, dt2dot,  instrumentnum, 64, vol)
			time = time + dt2dot
			add_note(time, dt2dot,  instrumentnum, 67, vol)
			time = time + dt2dot
			add_note(time, dt2+dt8, instrumentnum, 72, vol)
			time = time + dt2dot
		end
	end ,
	[30] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time, dt1+dt4dot, instrumentnum, 72, vol)
			time = time + dt1dot
		end
	end ,
	[31] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time, dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time, dt16, instrumentnum, 65, vol)
			time = time + dt16
			add_note(time, dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time, dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time, dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time, dt16, instrumentnum, 71, vol)
			time = time + dt16
		end
	end ,
	[32] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time, dt16, instrumentnum, 65, vol)
			time = time + dt16
			add_note(time, dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time, dt16, instrumentnum, 65, vol)
			time = time + dt16
			add_note(time, dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time, dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time, dt2dot+dt16, instrumentnum, 65, vol)
			time = time + dt2dot + dt16
			add_note(time, dt4+dt16, instrumentnum, 67, vol)
			time = time + dt4dot
		end
	end ,
	[33] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time, dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time, dt16, instrumentnum, 65, vol)
			time = time + dt8dot
		end
	end ,
	[34] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time, dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time, dt16, instrumentnum, 65, vol)
			time = time + dt16
		end
	end ,
	[35] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time, dt16, instrumentnum, 65, vol)
			time = time + dt16
			add_note(time, dt16, instrumentnum, 67, vol)
			time = time + dt16
			for i = 1,4 do
				add_note(time, dt16, instrumentnum, 71, vol)
				time = time + dt16
				add_note(time, dt16, instrumentnum, 67, vol)
				time = time + dt16
			end
			time = time + dt8 + dt2dot
			add_note(time, dt4, instrumentnum, 70, vol)
			time = time + dt4
			add_note(time, dt2dot, instrumentnum, 79, vol)
			time = time + dt2dot
			add_note(time, dt8, instrumentnum, 81, vol)
			time = time + dt8
			add_note(time, dt4, instrumentnum, 79, vol)
			time = time + dt4
			add_note(time, dt8, instrumentnum, 83, vol)
			time = time + dt8
			add_note(time, dt4dot, instrumentnum, 81, vol)
			time = time + dt4dot
			add_note(time, dt8, instrumentnum, 79, vol)
			time = time + dt8
			add_note(time, dt2dot, instrumentnum, 76, vol)
			time = time + dt2dot
			add_note(time, dt8, instrumentnum, 79, vol)
			time = time + dt8
			add_note(time, dt8+dt2dot, instrumentnum, 78, vol)
			time = time + dt8+dt2dot+dt2+dt8
			add_note(time, dt8+dt2, instrumentnum, 76, vol)
			time = time + dt8+dt2
			add_note(time, dt1dot, instrumentnum, 77, vol)
			time = time + dt1dot
		end
	end ,
	[36] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 65, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
		end
	end ,
	[37] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 65, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
		end
	end ,
	[38] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 65, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
		end
	end ,
	[39] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 65, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 72, vol)
			time = time + dt16
		end
	end ,
	[40] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 65, vol)
			time = time + dt16
		end
	end ,
	[41] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 71, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
		end
	end ,
	[42] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt1, instrumentnum, 72, vol)
			time = time + dt1
			add_note(time,   dt1, instrumentnum, 71, vol)
			time = time + dt1
			add_note(time,   dt1, instrumentnum, 69, vol)
			time = time + dt1
			add_note(time, dt2dot+dt8, instrumentnum, 72, vol)
			time = time + dt1
		end
	end ,
	[43] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 77, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 76, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 77, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 76, vol)
			time = time + dt16
			for i = 1,3 do
				add_note(time,   dt8, instrumentnum, 69, vol)
				time = time + dt8
			end
			add_note(time,   dt16, instrumentnum, 77, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 76, vol)
			time = time + dt16
		end
	end ,
	[44] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,    dt8, instrumentnum, 77, vol)
			time = time + dt8
			add_note(time, dt8dot, instrumentnum, 76, vol)
			time = time + dt8
			add_note(time,    dt8, instrumentnum, 76, vol)
			time = time + dt8
			add_note(time,    dt4, instrumentnum, 72, vol)
			time = time + dt4
		end
	end ,
	[45] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,    dt4, instrumentnum, 74, vol)
			time = time + dt4
			add_note(time,    dt4, instrumentnum, 74, vol)
			time = time + dt4
			add_note(time,    dt4, instrumentnum, 67, vol)
			time = time + dt4
		end
	end ,
	[46] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		local function riff (time)
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 74, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 76, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 74, vol)
			time = time + dt16
		end
		while time < endtime do
			riff(time)
			time = time + dt8
			for i = 1,3 do
				add_note(time,  dt8, instrumentnum, 67, vol)
				time = time + dt8
			end
			riff(time)
		end
	end ,
	[47] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 74, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 76, vol)
			time = time + dt16
			add_note(time,    dt8, instrumentnum, 74, vol)
			time = time + dt8
		end
	end ,
	[48] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time, dt1+dt4dot, instrumentnum, 67, vol)
			time = time + dt1dot
			add_note(time,   dt1, instrumentnum, 67, vol)
			time = time + dt1
			add_note(time, dt1+dt8, instrumentnum, 65, vol)
			time = time + dt1 + dt4
		end
	end ,
	[49] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 65, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 70, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 70, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
		end
	end ,
	[50] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 65, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
		end
	end ,
	[51] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 65, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 70, vol)
			time = time + dt16
		end
	end ,
	[52] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 70, vol)
			time = time + dt16
		end
	end ,
	[53] = function (starttime, duration, instrumentnum)
		local time = starttime
		local endtime = starttime + duration
		local patch, vol = table.unpack(Instruments[instrumentnum])
		while time < endtime do
			add_note(time,   dt16, instrumentnum, 70, vol)
			time = time + dt16
			add_note(time,   dt16, instrumentnum, 67, vol)
			time = time + dt16
		end
	end ,
}

function the_pulse (starttime, duration)
	local piano = Instruments[0]
	local time = starttime
	while time < starttime+duration do
		-- {'note', start_time, duration, channel, note, velocity}
		add_event( {'note', time, Pulse, 0, 84, piano[2]} )
		add_event( {'note', time, Pulse, 0, 96, piano[2]} )
		time = time + Pulse
	end
	add_event( {'note', time, dt1, 0, 84, piano[2]-5} )
	add_event( {'note', time, dt1, 0, 96, piano[2]-5} )
	time = time + dt1
	return
end

SetupTime = 0
for i,instrument in pairs(Instruments) do   -- setup
	add_event( {'patch_change',   SetupTime, i, instrument[1]} )
	SetupTime = SetupTime + 3
	add_event( {'control_change', SetupTime, i, 64, 0} )
	SetupTime = SetupTime + 3
	if i > 0 then
		local pan = 127 * (i - 1) / (#Instruments - 1)
		add_event( {'control_change', SetupTime, i, 10, pan} )
		SetupTime = SetupTime + 3
	end
end

for instrumentnum,v in pairs(Instruments) do   -- the main loop
	if instrumentnum > 0 then
		local instrument_time = SetupTime + Pulse*rayleigh_irand(15)
		for p = 1,#Phrases do
			local start_delay = Pulse*(DelayFix + rayleigh_irand(DelayRand))
			local duration    = Pulse*(DuratFix + rayleigh_irand(DuratRand))
			local start_time  = instrument_time + start_delay
-- should  nudge  start_delay and duration  to finish at p*AveragePhraseTime
			Phrases[p](start_time, duration, instrumentnum)
			instrument_time   = start_time + duration
		end
		if instrument_time > Finished then Finished = instrument_time end
	end
end
if not Quiet then
	the_pulse(SetupTime, Finished - SetupTime + Pulse*15)
end

write_score()

--[=[

=pod

=head1 NAME

in_c.lua - generates a midi performance of Terry Riley's In C

=head1 SYNOPSIS

 lua in_c.lua > in_c.mid

=head1 DESCRIPTION

This script uses C<midigenerators.lua> to perform Terry Riley's "In C".

=head1 TERRY RILEY's PERFORMING DIRECTIONS

All performers play from the same page of 53 melodic patterns
  played in sequence.
Any number of any kind of instruments can play.  A group of about
  35 is desired if possible but smaller or larger groups will work.
If vocalist(s) join in they can use any vowel and consonant sounds they like.

Patterns are to be played consecutively with each performer having the
  freedom to determine how many times he or she will repeat each pattern
  before moving on to the next.
There is no fixed rule as to the number of repetitions a pattern may have,
  however, since performances normally average between 45 minutes and an
  hour and a half, it can be assumed that one would repeat each pattern
  from somewhere between 45 seconds and a minute and a half or longer.

It is very important that performers listen very carefully to one another
  and this means occasionally to drop out and listen.
As an ensemble, it is very desirable to play very softly as well
  as very loudly and to try to diminuendo and crescendo together.

Each pattern can be played in unison or canonically in any alignment
  with itself or with its neighboring patterns.
One of the joys of IN C is the interaction of the players in polyrhythmic
  combinations that spontaneously arise between patterns.
  Some quite fantastic shapes will arise and disintegrate as
  the group moves through the piece when it is properly played.
It is important not to hurry from pattern to pattern but to stay on a
  pattern long enough to interlock with other patterns being played.
As the performance progresses,
  performers should stay within 2 or 3 patterns of each other.
It is important not to race too far ahead or to lag too far behind.

The ensemble can be aided by the means of an eighth note pulse
  played on the high c’s of the piano or on a mallet instrument.
It is also possible to use improvised percussion in strict rhythm
  (drum set, cymbals, bells, etc.),
  if it is carefully done and doesn’t overpower the ensemble.
All performers must play strictly in rhythm and
  it is essential that everyone play each pattern carefully.
It is advised to rehearse patterns in unison before attempting to
  play the piece, to determine that everyone is playing correctly.

The tempo is left to the discretion of the performers, obviously
  not too slow, but not faster than performers can comfortably play.
It is important to think of patterns periodically so that when you are
  resting you are conscious of the larger periodic composite accents that
  are sounding, and when you re-enter you are aware of what effect your
  entrance will have on the music’s flow.

The group should aim to merge into a unison at least once or twice
  during the performance.  At the same time, if the players seem to be
  consistently too much in the same alignment of a pattern, they should
  try shifting their alignment by an eighth note or quarter note with
  what’s going on in the rest of the ensemble.
It is OK to transpose patterns by an octave, especially to transpose up.
Transposing down by octaves works best on
  the patterns containing notes of long durations.
Augmentation of rhythmic values can also be effective.
If for some reason a pattern can’t be played,
  the performer should omit it and go on.
Instruments can be amplified if desired.
Electronic keyboards are welcome also.

IN C is ended in this way:  when each performer arrives at figure #53,
  he or she stays on it until the entire ensemble has arrived there.
  The group then makes a large crescendo and diminuendo a few times
  and each player drops out as he or she wishes.

Terry Riley

=head1 ARGUMENTS

=over 3

=item I<-m 22>

Sets the approximate duration in B<M>inutes.
The default is approximately 15 minutes.

=item I<-p 280>

This sets the B<P>ulse-duration in milliseconds.
The default is 262 which is the average of the available recordings.
The available recordings range from 227 to 353 milliseconds.

=item I<-q>

This B<Q>uietens (suppresses) the optional piano-pulse.

=item I<-s 999>

This set the B<S>eed of the random-number-generator.
By default is set to a somewhat unpredictable value.

=item I<-v>

Print the Version

=back

=head1 DOWNLOAD

This at is available at C<~/mus/arr/terry_riley/in_c.lua>

=head1 AUTHOR

Peter J Billam, http://pjb.com.au/comp/contact.html

=head1 SEE ALSO

 http://pjb.com.au/

=cut

]=]
