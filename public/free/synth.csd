<CsoundSynthesizer>
<CsOptions>
-d -m0 -M0 -odac
</CsOptions>
<CsInstruments>
;Example by Iain McCurdy

sr     = 44100
ksmps  = 32
nchnls = 2
0dbfs  = 1

;  ftgen ifn, itime, isize, igen, <GEN routine args...>
giSine     ftgen    0, 0, 4096, 10, 1
giSqua65   ftgen    0, 0, 1024, 7, 0, 90, 1, 332, 1, 180, -1, 332, -1, 90, 0
giSqua90   ftgen    0, 0, 1024, 7, 0, 26, 1, 460, 1,  52, -1, 460, -1, 26, 0
giSqua     ftgen    0, 0, 1024, 7, 0,  1, 1, 510, 1,   2, -1, 510, -1,  1, 0
giTria     ftgen    0, 0, 1024, 7, 0, 256,  1, 512, -1, 256, 0
giSaw75up  ftgen    0, 0, 1024, 7, 0, 384,  1, 256, -1, 384, 0
giSaw75do  ftgen    0, 0, 1024, 7, 0, 384, -1, 256,  1, 384, 0
giSaw90up  ftgen    0, 0, 1024, 7, 0, 461,  1, 102, -1, 461, 0
giSaw90do  ftgen    0, 0, 1024, 7, 0, 461, -1, 102,  1, 461, 0
giSawup    ftgen    0, 0, 1024, 7, 0, 512,  1,   0, -1, 512, 0
giSawdo    ftgen    0, 0, 1024, 7, 0, 512, -1,   0,  1, 512, 0
giFatSine  ftgen    0, 0, 4096, 8, 0, 256,.6186, 256,.8409, 256,.9612, \
 256,1, 256,.9612, 256,.8409, 256,.6186, 256,0, 256,-.6186, 256,-.8409, \
 256,-.9612, 256,-1, 256,-.9612, 256,-.8409, 256,-.6186, 256,0 ; sqrt of sin
giThinSin3 ftgen    0, 0, 4096, 10, 0.75, 0, -0.25
giThinSin5 ftgen    0, 0, 4096, 10, 0.6522, 0, -0.2174, 0, .1304
giSqua5    ftgen    0, 0, 4096, 10, 0.75, 0, 0.25, 0, .14
giSqua7    ftgen    0, 0, 4096, 10, 0.75, 0, 0.25, 0, .15, 0, .1
; sawtooth B(n) = 2 * -1^(n+1) / n
; triangle B(n) = 1*sin(x) - 1/3^2 * six(3x) + 1/5^2 * sin(5x) - ...
; square   B(n) = 1*sin(x) + 1/3   * six(3x) + 1/5   * sin(5x) - ...
giMT_Tri   ftgen    0, 0, 1024, 23, "MT_Tri.txt"
giRabTri   ftgen    0, 0, 8193, 23, "RabTri.txt"
giRabSqu   ftgen    0, 0, 8193, 23, "RabSqu.txt"


; Apparently these work only for realtime-midi, not for file-midi...
pgmassign 0,0
massign 1,1
massign 2,2
massign 3,3
massign 4,4
massign 5,5
massign 6,6
massign 7,7
massign 8,8
massign 9,9
massign 10,10
massign 11,11
massign 12,12
massign 13,13
massign 14,14
massign 15,15
massign 16,16

; mididefault 42, pN only has access to p4-p9; 6 variables.
; but there are 127 controllers, plus patch, bend etc :-(
; Hmm... could invoke the instruments in an initialisation-mode,
; right at the beginning of CsScore with p4 set somehow; then they
; could set all their iPatch kPan etc to initial values. These would
; then be in existence, allowing midicontrolchange 10, kPan statements.
; Disadvantage: one extra level of indentation, no big deal.
; But would initialisation be finished before the first midi event arrives?
; Yes, in practice; but there's a big unreliable-persistence-problem :-(

; doc: "initc7 can be used together with both midic7 and ctrl7 opcodes
;  for initializing the first controller's value."
; but it also works with midicontrolchange ....
#define InitMidiControllers(CHA)  #
  initc7  $CHA,  1, 0.8  ; Modulation
  initc7  $CHA,  5, 0.3  ; Portamento-time
  initc7  $CHA,  6,.01182; Data-entry MSB
  initc7  $CHA,  7, 0.85 ; Channel-volume
  initc7  $CHA, 10, 0.5  ; Pan
  initc7  $CHA, 11, 0.85 ; Expression
  initc7  $CHA, 65, 0    ; Portamento-on-off
  initc7  $CHA, 72, 0.03 ; Release-time
  initc7  $CHA, 73, 0.05 ; Attack-time
  initc7  $CHA, 74, 0.99 ; Cut-off Frequency
  initc7  $CHA, 75, 1.0  ; Decay-time
  initc7  $CHA, 76, 0.5  ; Vibrato-rate
  initc7  $CHA, 77, 0    ; Vibrato-depth
  initc7  $CHA, 78, 0.5  ; Vibrato-delay
  initc7  $CHA, 84, 0    ; Portamento-control
  initc7  $CHA, 85, 0    ; LFO-channel (10..15; default 0=off) NON-STANDARD
  initc7  $CHA, 86, 0    ; the MIDI-controller we connect it to NON-STANDARD
  initc7  $CHA, 87, 0    ; MIDI-controller's minimum value NON-STANDARD
  initc7  $CHA, 88, 1.0  ; MIDI-controller's maximum value NON-STANDARD
  initc7  $CHA, 90, 0.497; Tremolo-rate NON-STANDARD
  initc7  $CHA, 91, 0.0  ; Reverb-depth
  initc7  $CHA, 92, 0.0  ; Tremolo-depth
  initc7  $CHA, 95, 0.0  ; Phaser-depth
  initc7  $CHA,100, 0    ; Registered Parameter LSB
#
$InitMidiControllers(1)
$InitMidiControllers(2)
$InitMidiControllers(3)
$InitMidiControllers(4)
$InitMidiControllers(5)
$InitMidiControllers(6)
$InitMidiControllers(7)
$InitMidiControllers(8)
$InitMidiControllers(9)
$InitMidiControllers(11)
$InitMidiControllers(12)
$InitMidiControllers(13)
$InitMidiControllers(13)
$InitMidiControllers(14)
$InitMidiControllers(15)
$InitMidiControllers(16)

; for each channel, need a TVF envelope (p58) and a TVA envelope (p60)
; TVF is L0 T1 L1 T2 L2 T3 L3 T4 L4
; TVA is    T1 L1 T2 L2 T3 L3 T4     and L0 and L4 are zero

#define GetController(CC'VAR'MIN'MAX) #
  midicontrolchange $CC, $VAR, 0, 127 ; fetch anyway, will override if cc85
  ; prints "VAR=$VAR gkCC85_$CHA=%g gkCC86_$CHA=%g gkCC87_$CHA=%g gkCC88_$CHA=%g%n", \
  ; gkCC85_$CHA, gkCC86_$CHA, gkCC87_$CHA, gkCC88_$CHA
  if gkCC86_$CHA == $CC then
    kinrange = 1
    if gkCC85_$CHA == 10 then
      $VAR = gkLFO_11
    elseif gkCC85_$CHA == 11 then
      $VAR = gkLFO_12
    elseif gkCC85_$CHA == 12 then
      $VAR = gkLFO_13
    elseif gkCC85_$CHA == 13 then
      $VAR = gkLFO_14
    elseif gkCC85_$CHA == 14 then
      $VAR = gkLFO_15
    elseif gkCC85_$CHA == 15 then
      $VAR = gkLFO_16
    else
      kinrange = 0
    endif
    if kinrange > 0.5 then
      ; scale to between CC87 and CC88 (broadest would be 0..127)
      $VAR = gkCC87_$CHA + 0.5*($VAR+1) * (gkCC88_$CHA-gkCC87_$CHA)
    endif
  endif
  ; scale 0..127 to between MIN and MAX
  $VAR = $MIN + $VAR * ($MAX-$MIN) / 128
#

#define GetPitchBend(VAR) #
  midipitchbend $VAR         ; doc says 0 to 127; in fact -1 to +1
  if gkCC86_$CHA < 0.5 then  ; cc86==0 is special-cased to mean PitchBend!
    if gkCC85_$CHA == 10 then
      $VAR = gkLFO_11
    elseif gkCC85_$CHA == 11 then
      $VAR = gkLFO_12
    elseif gkCC85_$CHA == 12 then
      $VAR = gkLFO_13
    elseif gkCC85_$CHA == 13 then
      $VAR = gkLFO_14
    elseif gkCC85_$CHA == 14 then
      $VAR = gkLFO_15
    elseif gkCC85_$CHA == 15 then
      $VAR = gkLFO_16
    endif
  endif
#

#define AudioChannel(CHA)  #
  ; p1 is channel-number 1-16, p2 is time, p3 (duration) is -1 meaning Held
  ; prints "p4=%g%n", p4
  if p4 == 1 then  ; initialisation, create vars to midiprogramchange later
    ; BIG PROBLEM... Persistence of vars is not guaranteed.  If a second
    ; note starts on the same instr before the first one has finished,
    ; then it seems to get a new instance, in new memory,  which does
    ; not include copies of the values of the variables :-( That's a BUG.
    ; FALSE REMEDY... init can initialise svars as well as kvars and avars
    ; but it reinitialises them every note :-(
    ; KLUDGE :-) could create global variables to do the persistence ?
	; KLUDGE PROBLEM... it only works for gi variables, in spite of kr.html
    ; KLUDGE SOLUTION :-) it works for gk too iff the RHS only contains k's
    iOct           = 5.0   ; midi pitch in Csound's 'oct' format
    iAmp           = 100   ; note-on event velocity parameter
    iMod           = 0     ; cc1 Modulation
    gkBenRan_$CHA  = 2     ; cc6 Parameter-data; pitch bend range in semitones
    gkVol_$CHA     = 0.85  ; cc7 Channel-volume
    giPatch_$CHA   = 1
    gkPan_$CHA     = 0.5   ; cc10 Pan
    ; kLPan          = 0.701
    ; kRPan          = 0.701
    giAttack_$CHA  = 13    ; cc73
    giDecay_$CHA   = 1     ; cc75
    giRelease_$CHA = 0.03  ; cc72
    gkFilNot_$CHA  = 127   ; cc74 Cut-off Frequency
    gkVibRat_$CHA  = 6     ; cc76 4 to 8 Hz
    gkVibDep_$CHA  = 0     ; cc77 0 to 1
    giVibDel_$CHA  = 1     ; cc78 0.1 to 10 sec logarithmic
    giPreOct_$CHA  = 0     ; previous pitch, for portamento purposes
    giPorTim_$CHA  = 0.3   ; cc5  0 to 2 sec
    giPortOn_$CHA  = 0     ; cc65 >63 means ON
    giPorMod_$CHA  = 0     ; cc84 defines the portamento start-note
    gkCC85_$CHA    = 0     ; LFO-channel (10..15; default 0=off) NON-STANDARD
    gkCC86_$CHA    = 0     ; the MIDI-controller we connect it to NON-STANDARD
    gkCC87_$CHA    = 0     ; MIDI-controller's minimum value NON-STANDARD
    gkCC88_$CHA    = 0     ; MIDI-controller's maximum value NON-STANDARD
    gkRevDep_$CHA  = 0     ; cc91
    giTreRat_$CHA  = 5     ; Hz NON-STANDARD cc90
    gkTreDep_$CHA  = 0     ; cc92
    gkPhaDep_$CHA  = 0     ; cc95
    gkRegPar_$CHA  = 127   ; cc100 Registered-parameter LSB
    gkBend_$CHA    = 0
    p4             = 0     ; get rid of it, it can be persistent :-)
    if p1 == 1 then
      gkLFO_11     = 0.0
      gkLFO_12     = 0.0
      gkLFO_13     = 0.0
      gkLFO_14     = 0.0
      gkLFO_15     = 0.0
      gkLFO_16     = 0.0
    endif
  else

    midinoteonoct iOct, iAmp
    iAmp = iAmp/127

    midicontrolchange 85, gkCC85_$CHA
    midicontrolchange 86, gkCC86_$CHA
    midicontrolchange 87, gkCC87_$CHA
    midicontrolchange 88, gkCC88_$CHA
    $GetController(7'gkVol_$CHA'0'1) ; Channel-volume, re-ranged 0 to 1
    kPortTime linseg  0, 0.001, 0.01 ; a value that quickly ramps up to 0.01
    kVol      portk   gkVol_$CHA, kPortTime ; create kVol smoothed using portk
    aVol      interp  kVol ; create an a-rate kVol, smoothed even further

    midiprogramchange giPatch_$CHA

    $GetController(10'gkPan_$CHA'0'1)
    ; printks "1 gkPan_$CHA=%g%n", 2, gkPan_$CHA
    kTrigger  changed  gkPan_$CHA ; if it changes recalculate kLPan, kRPan
    if kTrigger=1 then
      kLPan      = sqrt(1.0-gkPan_$CHA)
      kRPan      = sqrt(gkPan_$CHA)
    endif

    ; The normal MIDI-Envelope
    midicontrolchange  73, giAttack_$CHA, 0, 1
    iAttack            = 10 * giAttack_$CHA * giAttack_$CHA
    midicontrolchange  75, giDecay_$CHA,  0, 1
    iDecay             = 3 * giDecay_$CHA
    midicontrolchange  72, giRelease_$CHA, 0, 1
    iRelease           = 10 * giRelease_$CHA
    ; prints "iAttack=%g iDecay=%g iRelease=%g%n", iAttack,iDecay,iRelease
    if giDecay_$CHA > 0.99 then
      kEnv     linsegr  0, iAttack,1, iRelease,0
    else
      kEnv     linsegr  0, iAttack,1, iDecay,0.707, iDecay,0.5, iDecay,0.353, \
               iDecay,0.250, iDecay,0.177, iDecay,0.125, iDecay,0.088, \
               iDecay,0.062, iDecay,0.044, iDecay,0.031, iDecay,0.022, \
               iDecay,0.015, iDecay,0.011, iDecay,0.007,  iRelease,0
    endif
    aEnv     interp   kEnv ; create a smoothed a-rate envelope

    ; Filter Cut-Off Frequency
    $GetController(74'gkFilNot_$CHA'0'127)
    kFilFre =  30 * cpsmidinn(gkFilNot_$CHA) ^ 0.7

    ; Vibrato
    ; midicontrolchange  76, gkVibRat_$CHA, 4, 8  ; Hz
    $GetController(76'gkVibRat_$CHA'4'8)  ; Hz
    ; midicontrolchange  77, gkVibDep_$CHA, 0, 1  ; max nearly 2 semitones
    $GetController(77'gkVibDep_$CHA'0'1)  ; max nearly 2 semitones
    midicontrolchange  78, giVibDel_$CHA, 0, 1
    iVibDel     =     0.1 * 100^giVibDel_$CHA   ; sec
    kVibEnv     linseg  0, 0.7*iVibDel, 0, 0.6*iVibDel, 0.1, 1, 0.1
    ; kVib      vibr    gkVibDep_$CHA, gkVibRat_$CHA, giSine ; too irregular
    kVib        oscil   gkVibDep_$CHA, gkVibRat_$CHA, giSine

    ; Portamento
    midicontrolchange   5, giPorTim_$CHA, 0,2   ; sec
    midicontrolchange  65, giPortOn_$CHA, 0,127
    midicontrolchange  84, giPorMod_$CHA, 0,127 ; portamento start-note 0-127
    if giPortOn_$CHA > 63.5 then
      if giPreOct_$CHA < 1 then
        giPreOct_$CHA = iOct
      endif
      if giPorMod_$CHA > 1.5 then
        kPorEnv linseg  octmidinn(giPorMod_$CHA), giPorTim_$CHA, iOct, 1, iOct
      else
        kPorEnv linseg  giPreOct_$CHA, giPorTim_$CHA, iOct, 1, iOct
      endif
    else
      kPorEnv   = iOct
    endif
    giPreOct_$CHA = iOct

    ; Reverb
    midicontrolchange  91, gkRevDep_$CHA, 0, 8

    ; Tremolo (use cc90 to mean Tremolo Rate)
    ; midicontrolchange  90, giTreRat_$CHA, 2, 8   ; Hz NON-STANDARD
    $GetController(90'gkTreRat_$CHA'2'8)  ; Hz NON-STANDARD
    ; midicontrolchange  92, gkTreDep_$CHA, 0, 1
    $GetController(92'gkTreDep_$CHA'0'1)
    kTre        oscil   gkTreDep_$CHA, giTreRat_$CHA, giSine
    kTre        = kTre + 1

    ; Phaser
    midicontrolchange  95, gkPhaDep_$CHA, 0, 0.95
    kPhaLin     expseg 1, 5, .005

    ; Pitch Bend
    ;midicontrolchange  100, gkRegPar_$CHA    ; Registered-parameter LSB
    ;if gkRegPar_$CHA < 1 then
      midicontrolchange  6, gkBenRan_$CHA    ; Data, here PitchBendRange
    ;endif
    kBend       = gkBend_$CHA
    $GetPitchBend(kBend)
    kBend       = (round(gkBenRan_$CHA) * kBend) / 12
    kBend       portk   kBend, kPortTime ; smoothed using portk
    gkBend_$CHA = kBend

	aAmp        init iAmp         ; a-rate otherwise the product is wrong ?!
    aAmp2       = aAmp*aVol*aEnv*kTre
    aFre        = cpsoct(kPorEnv+kBend) * (1.0 + kVibEnv*kVib)
    if giPatch_$CHA == 0 then ; Sine
      aSig      oscil   0.15*aAmp2, aFre, giSine
    elseif giPatch_$CHA == 1 then ; Square 65%
      aSig      oscil   0.12*aAmp2, aFre, giSqua65
    elseif giPatch_$CHA == 2 then ; Square 90%
      aSig      oscil   0.12*aAmp2, aFre, giSqua90
    elseif giPatch_$CHA == 3 then ; Square
      aSig      oscil   0.12*aAmp2, aFre, giSqua
    elseif giPatch_$CHA == 4 then ; Triangle
      aSig      oscil   0.15*aAmp2, aFre, giTria
    elseif giPatch_$CHA == 5 then ; Intermediate Triangle/Sawtooth wave 75%
      aSig      oscil   0.16*aAmp2, aFre, giSaw75up
    elseif giPatch_$CHA == 6 then ; Intermediate Triangle/Sawtooth wave 90%
      aSig      oscil   0.16*aAmp2, aFre, giSaw90up
    elseif giPatch_$CHA == 7 then ; Sawtooth wave Up
      aSig      oscil   0.17*aAmp2, aFre, giSawup
    elseif giPatch_$CHA == 8 then ; Buzz
      kMul      aftouch 0.4, 0.85       ; used as a kind of gbuzz tone control
      kMul      portk   kMul, kPortTime ; a new smoothed kBend
      aSig  gbuzz 0.1*aAmp2, aFre, 70, 0, kMul, giSine
    elseif giPatch_$CHA == 9 then ; 
      aSig      oscil   0.16*aAmp2, aFre, giSqua5
    elseif giPatch_$CHA == 10 then ; 
      aSig      oscil   0.16*aAmp2, aFre, giSqua7
    elseif giPatch_$CHA == 11 then ; 
      aSig1     oscil   0.16*aAmp2, aFre, giSine
      aSig2     oscil   0.16*aAmp2, 2.0*aFre, giSine
      aSig4     oscil   0.16*aAmp2, 4.0*aFre, giSine
      aSig      = 0.80*aSig1 + 0.10*aSig2 + 0.20*aSig4
    elseif giPatch_$CHA == 12 then ; 
      aSig      oscili   0.12*aAmp2, aFre, giFatSine
    elseif giPatch_$CHA == 13 then ; 
      aSig      oscili   0.15*aAmp2, aFre, giThinSin3
    elseif giPatch_$CHA == 14 then ; 
      aSig      oscili   0.15*aAmp2, aFre, giThinSin5
    elseif giPatch_$CHA == 15 then ; 
      aSig      oscili   0.15*aAmp2, aFre/171, giMT_Tri
    elseif giPatch_$CHA == 16 then ; 
      aSig      oscili   0.15*aAmp2, aFre/1217.75, giRabTri
    elseif giPatch_$CHA == 17 then ; 
      aSig      oscil    0.15*aAmp2, aFre/1217.75, giRabSqu
    else  ; default
      aSig      oscil   0.15*aAmp2, aFre, giTria
    endif
    if kFilFre < 20000 then
      aSig        lowpass2  aSig, kFilFre, 2.5
    endif
    ;if gkRevDep_$CHA > 0.05 then
      aSig        reverb aSig, gkRevDep_$CHA
    ;endif
    if gkPhaDep_$CHA > 0.03 then  ; BUG in phaser2 if >7 notes phased at once!
      aPha        phaser2 aSig, kPhaLin * 2000, .7, 8, 0.9, 2, .33
      aSig        = aSig + gkPhaDep_$CHA * aPha
    endif
    outs        kLPan*aSig, kRPan*aSig
  endif
#

#define LfoChannel(CHA)  #
  ; p1 is channel-number 1-16, p2 is time, p3 (duration) is -1 meaning Held
  if p4 == 1 then  ; initialisation, create vars to midiprogramchange later
    iOct           = 5.0   ; midi pitch in Csound's 'oct' format
    iAmp           = 100   ; note-on event velocity parameter
    giPatch_$CHA   = 100   ; default Sine
    gkLatestNote_$CHA = 0  ; fudge to get Mono behaviour
    p4             = 0     ; get rid of it, it can be persistent :-)
  else
    midinoteonoct  iOct, iAmp
    iFre           = cpsoct(iOct) / 256
    midiprogramchange giPatch_$CHA
    ; prints "LfoCha $CHA p1=%g giPatch_$CHA=%g iFre=%g%n",p1,giPatch_$CHA,iFre
    ; compare gkLatestNote_$CHA and kThisNote to imitate Mono operation
    kThisNote      init 0
    if kThisNote < 0.5 then
      gkLatestNote_$CHA = gkLatestNote_$CHA + 1
      kThisNote    = gkLatestNote_$CHA
    endif
    if kThisNote == gkLatestNote_$CHA then  ; enforce Monophonic behaviour
      if giPatch_$CHA == 100 then
        gkLFO_$CHA   oscil   1, iFre, giSine
      elseif giPatch_$CHA == 101 then
        gkLFO_$CHA   oscil   1, iFre, giTria
      elseif giPatch_$CHA == 102 then
        gkLFO_$CHA   oscil   1, iFre, giSqua65
      elseif giPatch_$CHA == 103 then
        gkLFO_$CHA   oscil   1, iFre, giSqua90
      elseif giPatch_$CHA == 104 then
        gkLFO_$CHA   oscil   1, iFre, giSqua
      elseif giPatch_$CHA == 105 then
        gkLFO_$CHA   oscil   1, iFre, giSawup
      elseif giPatch_$CHA == 106 then
        gkLFO_$CHA   oscil   1, iFre, giSaw90up
      elseif giPatch_$CHA == 107 then
        gkLFO_$CHA   oscil   1, iFre, giSaw75up
      elseif giPatch_$CHA == 108 then
        gkLFO_$CHA   oscil   1, iFre, giTria
      elseif giPatch_$CHA == 100 then
        gkLFO_$CHA   oscil   1, iFre, giSaw75do
      elseif giPatch_$CHA == 110 then
        gkLFO_$CHA   oscil   1, iFre, giSaw90do
      elseif giPatch_$CHA == 111 then
        gkLFO_$CHA   oscil   1, iFre, giSawdo
      endif
    endif
  endif
#

instr 1    ; channel 0
  $AudioChannel(1)
endin
instr 2    ; channel 1
  $AudioChannel(2)
endin
instr 3    ; channel 2
  $AudioChannel(3)
endin
instr 4    ; channel 3
  $AudioChannel(4)
endin
instr 5    ; channel 4
  $AudioChannel(5)
endin
instr 6    ; channel 5
  $AudioChannel(6)
endin
instr 7    ; channel 6
  $AudioChannel(7)
endin
instr 8    ; channel 7
  $AudioChannel(8)
endin
instr 9    ; channel 8
  $AudioChannel(9)
endin

instr 10    ; channel 9 is the percussion, as in GM
  kkey      init 0
  kvelocity init 0
  midinoteonkey kkey, kvelocity
  kAmp      = kvelocity / 127

  kcontroller      init 0
  kcontrollervalue init 0
  midicontrolchange kcontroller, kcontrollervalue
  kVol      init 0.8
  kLPan     = 0.707
  kRPan     = 0.707
  if kcontroller == 7 then
    kVol    = kcontrollervalue / 127
  elseif kcontroller == 10 then
    kPan    = kcontrollervalue / 127
  endif
  kTrigger    changed  kPan ; if kPan changes, recalculate kLPan, kRPan
  if kTrigger == 1 then
    kLPan   = sqrt(1.0-kPan)
    kRPan   = sqrt(kPan)
  endif
  ; printf "kLPan=%g kRPan=%g%n", kPan, kLPan, kRPan

  if kkey == 35 then ; Acoustic Bass Drum
    kEnv    expseg 1, 0.1, .15, 1, .002
  	aSig    oscili  kAmp*kVol*kEnv, 50, giSine
  elseif kkey == 36 then ; Bass Drum 1
    kEnv    expseg 1, 0.1, .2, 1, .002
  	aSig    oscili  kAmp*kVol*kEnv, 65, giSine
  elseif kkey == 37 then ; Side Stick
    kEnv    expseg 1.1, 0.05, .001, 0.5, .00001
  	aSig    oscili  kAmp*kVol*kEnv, 900, giTria
  elseif kkey == 38 then ; Acoustic Snare
    kEnv    expseg 1, 0.1, .2, 0.2, .002
  	aSig    noise  kAmp*kVol*kEnv, 0.0
  elseif kkey == 40 then ; Electric Snare
    kEnv    expseg 1, 0.1, .2, 1, .002
  	aSig    noise  kAmp*kVol*kEnv, 0.7
  elseif kkey == 60 then ; High Bongo
    kEnv    expseg 1, 0.1, .04, 1, .00001
  	aSig    oscili  kAmp*kVol*kEnv, 410, giSine
  elseif kkey == 61 then ; Low Bongo
    kEnv    expseg 1, 0.1, .1, 1, .0001
  	aSig    oscili  kAmp*kVol*kEnv, 300, giSine
  elseif kkey == 62 then ; Mute High Conga
    kEnv    expseg 1, 0.1, .002, 0.5, .00001
  	aSig    oscili  kAmp*kVol*kEnv, 215, giSine
  elseif kkey == 63 then ; Open High Conga
    kEnv    expseg 1, 0.1, .10, 1, .00001
  	aSig    oscili  kAmp*kVol*kEnv, 205, giSine
  elseif kkey == 64 then ; Low Conga
    kEnv    expseg 1, 0.1, .12, 1, .0001
  	aSig    oscili  kAmp*kVol*kEnv, 150, giSine
  elseif kkey == 81 then ; Open Triangle
    kEnv    expsegr 0.3, 0.01, .15, 10, .002, 10, .0001
  	aSig    oscili  kAmp*kVol*kEnv, 4000, giSine
  endif
  outs      kLPan*aSig, kRPan*aSig
endin

instr 11   ; channel 10
  $LfoChannel(11)
endin
instr 12   ; channel 11
  $LfoChannel(12)
endin
instr 13   ; channel 12
  $LfoChannel(13)
endin
instr 14   ; channel 13
  $LfoChannel(14)
endin
instr 15   ; channel 14
  $LfoChannel(15)
endin
instr 16   ; channel 15
  $LfoChannel(16)
endin

</CsInstruments>
<CsScore>
i 1 0 0 1 ; inititialise;
i 2 0 0 1
i 3 0 0 1
i 4 0 0 1
i 5 0 0 1
i 6 0 0 1
i 7 0 0 1
i 8 0 0 1
i 9 0 0 1
i 11 0 0 1
i 12 0 0 1
i 13 0 0 1
i 14 0 0 1
i 15 0 0 1
i 16 0 0 1

f 0 300

e
</CsScore>
<CsoundSynthesizer>

/*

my $Version       = '1.0';
my $VersionDate   = '10nov2010';

=pod

=head1 NAME

synth.csd - a Csound script for a low-tech old-fashioned synth

=head1 SYNOPSIS

 csound -M 99 synth.csd    # to get a list of your MIDI-device numbers
 csound -M 6 synth.csd     # connect your MIDI-device 6 to the synth
 csound synth.csd          # connects MIDI-device 0 to the synth
 csound -O null synth.csd  # doesn't log to the screen
 csound -T -F in.mid synth.csd   # plays the MIFI-file in.mid

=head1 DESCRIPTION

This csound script takes MIDI input and produces audio output.

Channels 0-8 are dedicated to Audio signals.
The basic Patches are simple, old-fashioned waveforms
like sine, triangle, square, and sawtooth.
The Channel Controllers for these Audio channels
can be set by the normal methods,
or one of them can be driven by a built-in Low-Frequency-Oscillator
by using the non-standard controllers cc85 to cc88.

Channel 9 is dedicated to a low-tech percussion-set
using very simple waveforms like sine or white noise.
It is roughly modelled on General-MIDI Channel 9,
see http://www.pjb.com.au/muscript/gm.html#perc

Channels 10-15 are dedicated to the Low-Frequency-Oscillators,
where MIDI-note number 60 means not middle-C,
but 1 Cycle-per-second (i.e. eight octaves lower!).

=head1 PATCHES

=over 3

=item B<Audio Patches> for channels 0-8 (Note number 60 is middle-C)

  0 = Sine wave
  1 = Square wave 65%
  2 = Square wave 90%
  3 = Square wave
  4 = Triangle wave
  5 = Intermediate Triangle/Sawtooth wave 75%
  6 = Intermediate Triangle/Sawtooth wave 90%
  7 = Sawtooth wave
  8 = Buzz
  9 = Five harmonics of a Square wave
 10 = Seven harmonics of a Square wave
 12 = Fat Sine
 13 = Thin Sine with 3rd harmonic
 14 = Thin Sine with 3rd and 5th harmonics
 15 = Morse-Thue-fractal Triangle wave
 16 = Rabbit-fractal Triangle wave
 17 = Rabbit-fractal Square wave


Changing: see round line 327;
plan: Tri-Sin-Squ-Tri-Saw-Sin-Oddsines Buzz Fractals Audioin

=item B<LFO Patches> for channels 10-15 (Note number 60 is 1 cycle-per-second!)

The Low-Frequency-Oscillators live in Channels 10 and above,
and use Patches 100 and above.
Their frequencies are eight octaves lower than the audio Patches,
so that Note number 60 means 1 cycle-per-second.
These two data-items, the Patch and the "Note",
are the only two that Low-Frequency-Oscillators need.

 100 = Sine wave
 101 = Triangle wave
 102 = Square wave 65%
 103 = Square wave 90%
 104 = Square wave
 105 = Sawtooth wave Up
 106 = Intermediate Triangle/Sawtooth wave 90% Up
 107 = Intermediate Triangle/Sawtooth wave 75% Up
 108 = another Triangle wave
 109 = Intermediate Triangle/Sawtooth wave 75% Down
 110 = Intermediate Triangle/Sawtooth wave 90% Down
 111 = Sawtooth wave Down

=back

=head1 STANDARD CONTROLLER-NUMBERS

=over 3

See http://www.pjb.com.au/muscript/gm.html#cc

   1 = Modulation
   7 = Channel volume
   5 = Portamento time
   6 = Data Entry MSB
  10 = Pan
  11 = Expression
  38 = Data Entry LSB
  64 = Sustain pedal
  65 = Portamento on-off
  72 = Release time
  73 = Attack time
  74 = Filter frequency
  75 = Decay time
  76 = Vibrato rate
  77 = Vibrato depth
  78 = Vibrato delay
  84 = Portamento from-note
  91 = Reverb depth
  92 = Tremolo depth
  95 = Phaser depth
 100 = Registered Parameter LSB
 101 = Registered Parameter MSB

The Pitch-bend-range is a "Registered Parameter", and is adjusted by:

 cc101=0, cc100=0, cc6=0..24 semitones, cc38=0, cc101=127, cc100=127 

=back

=head1 NON-STANDARD CONTROLLER-NUMBERS

 85 = LFO-channel (10..15; default 0=off)
 86 = the audio Channel-Controller that the LFO will control
      (cc86=0 is special-cased to mean Pitch-Bend)
 87 = that audio Channel-Controller's minimum value
 88 = that audio Channel-Controller's maximum value

 90 = Tremolo rate

=head1 CHANGES

 20101122 cc6 used for PitchBendRange, but cc100,cc101 fail...
 20101121 cc65 doesnt cause spurious swoop
 20101121 fix spurious patch-to-instr assignment
 20101121 rework Decay to be a half-life, not lin
 20101120 phaser2 bug mostly avoided
 20101120 Portamento 84 ok but bug w>14 notes on 1 channel
 20101120 does cc5,65,84 Portamento
 20101119 does cc76,77,78 Vibrato
 20101118 Tremolo and Lowpass
 20101116 sketch in Portamento and Vibrato
 20101116 iPan doesn't update k's; back to kPan
 20101115 conquered some mididefault demons
 20101115 wrestling with nasty persistence problems
 20101115 working with mididefault pN
 20101114 uses patches; sounds not tied to channels
 20101114 more percussion instruments
 20101113 three sounds work already on channel 9
 20101112 now detects patch changes
 20101111 does amplitude envelope
 20101111 handles Pan controller :-)
 20101111 use #define to consistentise the channels
 20101110 sine, triangle, square, sawtooth, buzz
 20101110 1.0 first working version


=head1 BUGS

The I<phaser2> function seems to fail when more than six phased
notes are sounding simultaneously.

The Registered-parameter controllers cc101 and cc100
are handled by I<csound> in some apparently undocumented way,
so I<synth.csd> ignores them,
and treats every cc6 event as setting the Pitch-bend-range.
Therefore, attempts to set e.g.  the Modulation-wheel-range
or the Coarse-tuning will cause unexpected results.

=head1 AUTHOR

Peter J Billam   http://www.pjb.com.au/comp/contact.html


=head1 SEE ALSO

 http://www.csounds.com
 http://www.pjb.com.au
 http://www.pjb.com.au/muscript/gm.html
 http://www.pjb.com.au/midi/midikbd.html
 aconnect (1)

=cut

*/
