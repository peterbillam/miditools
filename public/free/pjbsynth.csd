<CsoundSynthesizer>
<CsOptions>
-d -m0 -M0 -iadc -odac
</CsOptions>
<CsInstruments>

sr     = 44100
ksmps  = 32
nchnls = 2
0dbfs  = 1

; ares delay asig, idlt

;  ftgen ifn, itime, isize, igen, <GEN routine args...>
giSine     ftgen    0, 0, 1024, 10, 1
giSqua65   ftgen    0, 0, 1024, 7, 0, 90, 1, 332, 1, 180, -1, 332, -1, 90, 0
giSqua90   ftgen    0, 0, 1024, 7, 0, 26, 1, 460, 1,  52, -1, 460, -1, 26, 0
giSqua     ftgen    0, 0, 1024, 7, 0,  1, 1, 510, 1,   2, -1, 510, -1,  1, 0
giTria     ftgen    0, 0, 1024, 7, 0, 256,  1, 512, -1, 256, 0
giSaw75up  ftgen    0, 0, 1024, 7, 0, 384,  1, 256, -1, 384, 0
giSaw75do  ftgen    0, 0, 1024, 7, 0, 384, -1, 256,  1, 384, 0
giSaw90up  ftgen    0, 0, 1024, 7, 0, 461,  1, 102, -1, 461, 0
giSaw90do  ftgen    0, 0, 1024, 7, 0, 461, -1, 102,  1, 461, 0
giSawup    ftgen    0, 0, 1024, 7, -1, 1022, 1,  2,-1
giSawdo    ftgen    0, 0, 1024, 7,  1, 1022,-1,  2, 1
giFatSine  ftgen    0, 0, 1024, 8, 0, 64,.6186, 64,.8409, 64,.9612, \
 64,1, 64,.9612, 64,.8409, 64,.6186, 64,0, 64,-.6186, 64,-.8409, \
 64,-.9612, 64,-1, 64,-.9612, 64,-.8409, 64,-.6186, 64,0 ; sqrt of sin
giRectSin  ftgen    0, 0, 1024, 8, -1, 64,-0.6936, 64,-0.3989, 64,-0.1273, \
 64,.1107, 64,.3061, 64,.4512, 64,.5406, 64,.5708, 64,.5406, 64,.4512, \
 64,.3061, 64,.1107, 64,-.1273, 64,-.3989, 64,-.6936, 64,-1 ; rect sin
giThinSin3 ftgen    0, 0, 1024, 10, 0.75, 0, -0.25
giThinSin5 ftgen    0, 0, 1024, 10, 0.6522, 0, -0.2174, 0, .1304
giSqua5    ftgen    0, 0, 1024, 10, 0.75, 0, 0.25, 0, .14
giSqua7    ftgen    0, 0, 1024, 10, 0.75, 0, 0.25, 0, .15, 0, .1
; sawtooth B(n) = 2 * -1^(n+1) / n
; triangle B(n) = 1*sin(x) - 1/3^2*six(3x) + 1/5^2*sin(5x) - ...
; square   B(n) = 1*sin(x) + 1/3  *six(3x) + 1/5  *sin(5x) - ...
giRabTri   ftgen    0, 0, 8193, 23, "RabTri.txt"
giRabSqu   ftgen    0, 0, 8192, 23, "RabSqu.txt"
giMT2Squ   ftgen    0, 0, 8193, 23, "MT2Squ.txt"
; tablekt — Provides k-rate control over table numbers.
; oscilikt — A linearly interpolated oscillator that allows changing the
;   table number at k-rate.
; osciliktp — A linearly interpolated oscillator that allows allows phase
;   modulation.
; lposcil - Read sampled sound (mono or stereo) from a table, with optional
; sustain and release looping, and high precision.
;   ares lposcil kamp, kfreqratio, kloop, kend, ifn [, iphs]
; oscili takes k params; can always freq-modulate...

giRab2fr   ftgen    0, 0, 1024, 7, 0, \
 7,1, 18,-1, 14,1, 14,-1, 18,1, 14,-1, 18,1, 14,-1, \
 14,1, 18,-1, 14,1, 14,-1, 18,1, \
 14,-1, 18,1, 14,-1, 14,1, 18,-1, 14,1, 18,-1, 14,1, \
 14,-1, 18,1, 14,-1, 14,1, 18,-1, 14,1, 18,-1, 14,1, \
 14,-1, 18,1, 14,-1, 14,1, 18,-1, \
 14,1, 18,-1, 14,1, 14,-1, 18,1, 14,-1, 18,1, 14,-1, \
 14,1, 18,-1, 14,1, 14,-1, 18,1, \
 14,-1, 18,1, 14,-1, 14,1, 18,-1, 14,1, 18,-1, 14,1, \
 14,-1, 18,1, 14,-1, 14,1, 18,-1, 14,1, 18,-1, 14,1, \
 14,-1, 18,1, 14,-1, 7,0

giMT_1fr   ftgen    0, 0, 1024, 7, 0, \
  7, 1, 17,-1, 17, 1, 15,-1, 17, 1, 15,-1, 15, 1, 17,-1, \
 15, 1, 17,-1, 17, 1, 15,-1, 17, 1, 15,-1, 15, 1, 17,-1, \
 15, 1, 17,-1, 17, 1, 15,-1, 17, 1, 15,-1, 15, 1, 17,-1, \
 15, 1, 17,-1, 17, 1, 15,-1, 17, 1, 15,-1, 15, 1, 17,-1, \
 15, 1, 17,-1, 17, 1, 15,-1, 17, 1, 15,-1, 15, 1, 17,-1, \
 15, 1, 17,-1, 17, 1, 15,-1, 17, 1, 15,-1, 15, 1, 17,-1, \
 15, 1, 17,-1, 17, 1, 15,-1, 17, 1, 15,-1, 15, 1, 17,-1, \
 15, 1, 17,-1, 17, 1, 15,-1, 17, 1, 15,-1, 15, 1, 17,-1, 8,0

giMT_2fr   ftgen    0, 0, 1024, 7, 0, \
  7, 1, 18,-1, 18, 1, 14,-1, 18, 1, 14,-1, 14, 1, 18,-1, \
 14, 1, 18,-1, 18, 1, 14,-1, 18, 1, 14,-1, 14, 1, 18,-1, \
 14, 1, 18,-1, 18, 1, 14,-1, 18, 1, 14,-1, 14, 1, 18,-1, \
 14, 1, 18,-1, 18, 1, 14,-1, 18, 1, 14,-1, 14, 1, 18,-1, \
 14, 1, 18,-1, 18, 1, 14,-1, 18, 1, 14,-1, 14, 1, 18,-1, \
 14, 1, 18,-1, 18, 1, 14,-1, 18, 1, 14,-1, 14, 1, 18,-1, \
 14, 1, 18,-1, 18, 1, 14,-1, 18, 1, 14,-1, 14, 1, 18,-1, \
 14, 1, 18,-1, 18, 1, 14,-1, 18, 1, 14,-1, 14, 1, 18,-1, 7,0

giPhaL     ftgen    0, 0, 4096, 8, 1, \
 128,.705, 128,.498, 128,.351, 128,.248, \
 128,.175, 128,.123, 128,.086, 128,.060, \
 128,.042, 128,.030, 128,.021, 128,.015, \
 128,.010, 128,.008, 128,.006, 128,.005, \
 128,.006, 128,.008, 128,.010, 128,.015, \
 128,.021, 128,.030, 128,.042, 128,.060, \
 128,.086, 128,.123, 128,.175, 128,.248, \
 128,.351, 128,.498, 128,.705, 128,1.00
giPhaR     ftgen    0, 0, 4096, 8, .005, \
 128,.006, 128,.008, 128,.010, 128,.015, \
 128,.021, 128,.030, 128,.042, 128,.060, \
 128,.086, 128,.123, 128,.175, 128,.248, \
 128,.351, 128,.498, 128,.705, 128,1.00, \
 128,.705, 128,.498, 128,.351, 128,.248, \
 128,.175, 128,.123, 128,.086, 128,.060, \
 128,.042, 128,.030, 128,.021, 128,.015, \
 128,.010, 128,.008, 128,.006, 128,.005

pgmassign 0,0
massign 1,1
massign 2,2
massign 3,3
massign 4,4
massign 5,5
massign 6,6
massign 7,7
massign 8,8
massign 9,9
massign 10,10
massign 11,11
massign 12,12
massign 13,13
massign 14,14
massign 15,15
massign 16,16

; mididefault 42, pN only has access to p4-p9; 6 variables.
; but there are 127 controllers, plus patch, bend etc :-(
; Hmm... could invoke the instruments in an initialisation-mode,
; right at the beginning of CsScore with p4 set somehow; then they
; could set all their iPatch kPan etc to initial values. These would
; then be in existence, allowing midicontrolchange 10, kPan statements.
; Disadvantage: one extra level of indentation, no big deal.
; But would initialisation be finished before the first midi event arrives?
; Yes, in practice; but there's a big unreliable-persistence-problem :-(

; doc: "initc7 can be used together with both midic7 and ctrl7 opcodes
;  for initializing the first controller's value."
; but it also works with midicontrolchange ....
#define InitMidiControllers(CHA)  #
  initc7  $CHA,  1, 0    ; Modulation
  initc7  $CHA,  5, 0.3  ; Portamento-time
  initc7  $CHA,  6,.01182; Data-entry MSB
  initc7  $CHA,  7, 0.93 ; Channel-volume
  initc7  $CHA, 10, 0.5  ; Pan
  initc7  $CHA, 11, 0.93 ; Expression
  initc7  $CHA, 20, 0    ; LFO-channel (10..15; default 0=off)  NON-STANDARD
  initc7  $CHA, 21, 0    ; the MIDI-controller we connect it to NON-STANDARD
  initc7  $CHA, 22, 0    ; MIDI-controller's minimum value NON-STANDARD
  initc7  $CHA, 23, 1.0  ; MIDI-controller's maximum value NON-STANDARD
  initc7  $CHA, 24, 0    ; LFO-channel (10..15; default 0=off)  NON-STANDARD
  initc7  $CHA, 25, 0    ; the MIDI-controller we connect it to NON-STANDARD
  initc7  $CHA, 26, 0    ; MIDI-controller's minimum value NON-STANDARD
  initc7  $CHA, 27, 1.0  ; MIDI-controller's maximum value NON-STANDARD
  initc7  $CHA, 44, 0    ; Loop Type     NON-STANDARD
  initc7  $CHA, 45, 0    ; Loop Volume   NON-STANDARD
  initc7  $CHA, 46, 0    ; Loop Delay 1  NON-STANDARD
  initc7  $CHA, 47, 0    ; Loop Delay 2  NON-STANDARD
  initc7  $CHA, 52, 0    ; Effect-Envelope Attack-Time (0=off)  NON-STANDARD
  initc7  $CHA, 53, 0    ; the MIDI-controller we connect it to NON-STANDARD
  initc7  $CHA, 54, 0    ; MIDI-controller's initial value NON-STANDARD
  initc7  $CHA, 55, 0.5  ; MIDI-controller's  final  value NON-STANDARD
  initc7  $CHA, 65, 0    ; Portamento-on-off
  initc7  $CHA, 71, 0.5  ; Filter Q
  initc7  $CHA, 72, 0.02 ; Release-time
  initc7  $CHA, 73, 0.02 ; Attack-time
  initc7  $CHA, 74, 0.99 ; Filter Frequency
  initc7  $CHA, 75, 1.0  ; Decay-time
  initc7  $CHA, 76, 0.5  ; Vibrato-rate
  initc7  $CHA, 77, 0    ; Vibrato-depth
  initc7  $CHA, 78, 0.5  ; Vibrato-delay
  initc7  $CHA, 84, 0    ; Portamento-control
  initc7  $CHA, 87, 0    ; Overdrive Distortion   NON-STANDARD
  initc7  $CHA, 88, 1.0  ; Ring Modulator Channel NON-STANDARD
  initc7  $CHA, 90, 0.497; Tremolo-rate NON-STANDARD
  initc7  $CHA, 91, 0.0  ; Reverb-depth
  initc7  $CHA, 92, 0.0  ; Tremolo-depth
  initc7  $CHA, 95, 0.0  ; Phaser-depth
  initc7  $CHA,100, 0    ; Registered Parameter LSB
#
$InitMidiControllers(1)
$InitMidiControllers(2)
$InitMidiControllers(3)
$InitMidiControllers(4)
$InitMidiControllers(5)
$InitMidiControllers(6)
$InitMidiControllers(7)
$InitMidiControllers(8)
$InitMidiControllers(9)
$InitMidiControllers(11)
$InitMidiControllers(12)
$InitMidiControllers(13)
$InitMidiControllers(13)
$InitMidiControllers(14)
$InitMidiControllers(15)
$InitMidiControllers(16)

; for each channel, need a TVF envelope (p58) and a TVA envelope (p60)
; TVF is L0 T1 L1 T2 L2 T3 L3 T4 L4
; TVA is    T1 L1 T2 L2 T3 L3 T4     and L0 and L4 are zero

;opcode mt2, k, k
;  ki xin
;  if (ki < 8 ) then
;    kb table ki, 91
;  else    ; can't get this to recurse at all; SEGFAULT.
;    kb1 mt2 ki % 8   ; should just use a big table.
;    kb2 mt2 floor(ki / 8)
;    kb = kb1 * kb2
;  endif
;  xout kb
;endop

#define GetController(CC'VAR'MIN'MAX) #
  ; midicontrolchange doesn't work in an if, so fetch anyway
  midicontrolchange $CC, $VAR, 0, 127
  ;  prints "VAR=$VAR gkCC20_$CHA=%g gkCC21_$CHA=%g gkCC22_$CHA=%g gkCC23_$CHA=%g%n", \
  ;  gkCC20_$CHA, gkCC21_$CHA, gkCC22_$CHA, gkCC23_$CHA
  if gkCC21_$CHA == $CC then
    kinrange = 1
    if gkCC20_$CHA == 10 then
      $VAR = gkLFO_11
    elseif gkCC20_$CHA == 11 then
      $VAR = gkLFO_12
    elseif gkCC20_$CHA == 12 then
      $VAR = gkLFO_13
    elseif gkCC20_$CHA == 13 then
      $VAR = gkLFO_14
    elseif gkCC20_$CHA == 14 then
      $VAR = gkLFO_15
    elseif gkCC20_$CHA == 15 then
      $VAR = gkLFO_16
    else
      kinrange = 0
    endif
    if kinrange > 0.5 then
      ; scale to between CC22 and CC23 (broadest would be 0..127)
      $VAR = gkCC22_$CHA + 0.5*($VAR+1) * (gkCC23_$CHA-gkCC22_$CHA)
    endif
  elseif gkCC25_$CHA == $CC then
    kinrange = 1
    if gkCC24_$CHA == 10 then
      $VAR = gkLFO_11
    elseif gkCC24_$CHA == 11 then
      $VAR = gkLFO_12
    elseif gkCC24_$CHA == 12 then
      $VAR = gkLFO_13
    elseif gkCC24_$CHA == 13 then
      $VAR = gkLFO_14
    elseif gkCC24_$CHA == 14 then
      $VAR = gkLFO_15
    elseif gkCC24_$CHA == 15 then
      $VAR = gkLFO_16
    else
      kinrange = 0
    endif
    if kinrange > 0.5 then
      ; scale to between CC26 and CC27 (broadest would be 0..127)
      $VAR = gkCC26_$CHA + 0.5*($VAR+1) * (gkCC27_$CHA-gkCC26_$CHA)
    endif
  elseif gkCC52_$CHA > 1  then  ; Effect-Envelope ?
  	if gkCC53_$CHA == $CC then
      ; kAttEnv linseg 1, gkCC52_$CHA, 0  ; BUG can't use k-rate idur1 :-(
      kTim   = gkCC52_$CHA * gkCC52_$CHA / 2000   ; square and scale to 0..4
      kAttEnv   portk  0, kTim, 1  ; exp rather than lin !?
      $VAR = gkCC55_$CHA - kAttEnv * (gkCC55_$CHA-gkCC54_$CHA)
      ; $VAR   portk  gkCC55_$CHA, kTim, gkCC54_$CHA ; p3 must not be k-rate
  	endif
  endif
  $VAR = $MIN + $VAR * ($MAX-$MIN) / 128   ; scale 0..127 to MIN..MAX
#

#define GetPitchBend(VAR) #
  midipitchbend $VAR         ; doc says 0 to 127; in fact -1 to +1
  kFound = 0
  if gkCC21_$CHA < 0.6 then  ; cc21==0 is special-cased to mean PitchBend!
    kFound = 1
    if gkCC20_$CHA == 10 then
      $VAR = gkLFO_11
    elseif gkCC20_$CHA == 11 then
      $VAR = gkLFO_12
    elseif gkCC20_$CHA == 12 then
      $VAR = gkLFO_13
    elseif gkCC20_$CHA == 13 then
      $VAR = gkLFO_14
    elseif gkCC20_$CHA == 14 then
      $VAR = gkLFO_15
    elseif gkCC20_$CHA == 15 then
      $VAR = gkLFO_16
    else
      kFound = 0
    endif
  elseif gkCC25_$CHA < 0.6 then  ; cc25==0 is special-cased to mean PitchBend!
    kFound = 1
    if gkCC24_$CHA == 10 then
      $VAR = gkLFO_11
    elseif gkCC24_$CHA == 11 then
      $VAR = gkLFO_12
    elseif gkCC24_$CHA == 12 then
      $VAR = gkLFO_13
    elseif gkCC24_$CHA == 13 then
      $VAR = gkLFO_14
    elseif gkCC24_$CHA == 14 then
      $VAR = gkLFO_15
    elseif gkCC24_$CHA == 15 then
      $VAR = gkLFO_16
    else
      kFound = 0
    endif
  endif
  if kFound == 0 then
    if gkCC52_$CHA > 1  then  ; Effect-Envelope ?
  	  if gkCC53_$CHA < 1 then     ; Bend?
        kTim   = gkCC52_$CHA * gkCC52_$CHA / 2000   ; square and scale to 0..4
        kAttEnv   portk  0, kTim, 1  ; exp rather than lin !?
        ; -1 to +1, remember...
        $VAR = -1 + (gkCC55_$CHA - kAttEnv * (gkCC55_$CHA-gkCC54_$CHA)) / 64
      endif
    endif
  endif
#

#define AudioChannel(CHA)  #
  ; p1 is channel-number 1-16, p2 is time, p3 (duration) is -1 meaning Held
  ; prints "p4=%g%n", p4
  if p4 == 1 then  ; initialisation, create vars to midiprogramchange later
    ; BIG PROBLEM... Persistence of vars is not guaranteed.  If a second
    ; note starts on the same instr before the first one has finished,
    ; then it seems to get a new instance, in new memory,  which does
    ; not include copies of the values of the variables :-( That's a BUG.
    ; FALSE REMEDY... init can initialise svars as well as kvars and avars
    ; but it reinitialises them every note :-(
    ; KLUDGE :-) could create global variables to do the persistence ?
    ; KLUDGE PROBLEM... it only works for gi variables, in spite of kr.html
    ; KLUDGE SOLUTION :-) it works for gk too iff the RHS only contains k's
    iOct           = 5.0   ; midi pitch in Csound's 'oct' format
    iAmp           = 100   ; note-on event velocity parameter
    gkMod_$CHA     = 0     ; cc1 Modulation
    kData          = 2     ; cc6 Parameter-data
    ;kDummy         = 2     ; throw away cc101
    gkBenRan_$CHA  = 2     ; pitch bend range in semitones
    gkVol_$CHA     = 0.93  ; cc7 Channel-volume
    giPatch_$CHA   = 1
    gkPan_$CHA     = 0.5   ; cc10 Pan
    gkExp_$CHA     = 0.93  ; cc11 Expression
    gkLooTyp_$CHA  = 0     ; Loop Type         NON-STANDARD cc44
    gkLooVol_$CHA  = 0     ; Loop Volume       NON-STANDARD cc45
    gkLooDel1_$CHA = 0     ; Loop Delay 1      NON-STANDARD cc46
    gkLooDel2_$CHA = 0     ; Loop Delay 2      NON-STANDARD cc47
    gkLooFlg_$CHA  = 1     ; flag for rabbit or mt2, switches between 1 and -1
    gkLooNum_$CHA  = 0     ; counter, used as arg for rabbit or mt2 opcodes
    gkLooTim_$CHA  = 0     ; timer, remembers the start of the current delay
    giAttack_$CHA  = 13    ; cc73
    giDecay_$CHA   = 1     ; cc75
    giRelease_$CHA = 0.03  ; cc72
    gkFilFre_$CHA  = 127   ; cc74 Filter Frequency
    gkFilQua_$CHA  = 63    ; cc71 Filter Q
    gkVibRat_$CHA  = 6     ; cc76 4 to 8 Hz
    gkVibDep_$CHA  = 0     ; cc77 0 to 1
    giVibDel_$CHA  = 1     ; cc78 0.1 to 10 sec logarithmic
    giPreOct_$CHA  = 0     ; previous pitch, for portamento purposes
    giPorTim_$CHA  = 0.3   ; cc5  0 to 2 sec
    giPortOn_$CHA  = 0     ; cc65 >63 means ON
    giPorMod_$CHA  = 0     ; cc84 defines the portamento start-note
    gkCC20_$CHA    = 0     ; LFO-channel (10..15; default 0=off)  NON-STANDARD
    gkCC21_$CHA    = 0     ; the MIDI-controller we connect it to NON-STANDARD
    gkCC22_$CHA    = 0     ; MIDI-controller's minimum value NON-STANDARD
    gkCC23_$CHA    = 0     ; MIDI-controller's maximum value NON-STANDARD
    gkCC24_$CHA    = 0     ; LFO-channel (10..15; default 0=off)  NON-STANDARD
    gkCC25_$CHA    = 0     ; the MIDI-controller we connect it to NON-STANDARD
    gkCC26_$CHA    = 0     ; MIDI-controller's minimum value NON-STANDARD
    gkCC27_$CHA    = 0     ; MIDI-controller's maximum value NON-STANDARD
    gkCC52_$CHA    = 0     ; Effect-Envelope Attack-Time     NON-STANDARD
    gkCC53_$CHA    = 0     ; the MIDI-controller we connect it to NON-STANDARD
    gkCC54_$CHA    = 0     ; MIDI-controller's minimum value NON-STANDARD
    gkCC55_$CHA    = 64    ; MIDI-controller's maximum value NON-STANDARD
    gkOvrDrv_$CHA  = 0  ; Overdrive Distortion   NON-STANDARD cc87
    gkRinMod_$CHA init 127 ; Ring Modulator Channel NON-STANDARD cc88
    ; prints "Init gkRinMod_$CHA=%g%n", gkRinMod_$CHA
    gkRevDep_$CHA  = 0     ; cc91
    gkTreRat_$CHA  = 5     ; Hz NON-STANDARD cc90
    gkTreDep_$CHA  = 0     ; cc92
    gkPhaDep_$CHA  = 0     ; cc95
    gkRegPar_$CHA  = 127   ; cc100 Registered-parameter LSB
    gkBend_$CHA    = 0
    kBend          = 0
    gaSig_$CHA     = 0     ; holds the dry signal so reverb can be added later
    p4             = 0     ; get rid of it, it can be persistent :-)
    if p1 == 1 then
      gkLFO_11     = 0.0
      gkLFO_12     = 0.0
      gkLFO_13     = 0.0
      gkLFO_14     = 0.0
      gkLFO_15     = 0.0
      gkLFO_16     = 0.0
    endif
  else

    midinoteonoct iOct, iAmp
    iAmp = iAmp/127

    midicontrolchange 20, gkCC20_$CHA
    midicontrolchange 21, gkCC21_$CHA
    midicontrolchange 22, gkCC22_$CHA
    midicontrolchange 23, gkCC23_$CHA
    midicontrolchange 24, gkCC24_$CHA
    midicontrolchange 25, gkCC25_$CHA
    midicontrolchange 26, gkCC26_$CHA
    midicontrolchange 27, gkCC27_$CHA
    midicontrolchange 52, gkCC52_$CHA
    midicontrolchange 53, gkCC53_$CHA
    midicontrolchange 54, gkCC54_$CHA
    midicontrolchange 55, gkCC55_$CHA
    $GetController(7'gkVol_$CHA'0'1)  ; Channel-volume, re-ranged 0 to 1
    $GetController(11'gkExp_$CHA'0'1) ; Expression
    kPortTime linseg  0, 0.001, 0.01  ; a value that quickly ramps up to 0.01
    ; kPortTime = 0.01  ; this clicks :-(  not sure I understand why...
    kVol      portk   gkVol_$CHA*gkExp_$CHA, kPortTime ; smoothed using portk
    aVol      interp  kVol ; create an a-rate kVol, smoothed even further

    midiprogramchange giPatch_$CHA
    ; stubbornly resets giPatch_$CHA=-1 before a ProgramChange has occurred
    if giPatch_$CHA < 0 then
      giPatch_$CHA = 0   ; Patch 0 is the default
    endif
    ;prints "giPatch_$CHA=%g%n", giPatch_$CHA ; maudio 1st P=2 seems to fail...

    ; Modulation
    $GetController(1'gkMod_$CHA'0'1)
    gkMod_$CHA  portk gkMod_$CHA, kPortTime
    ; tabmorph just gets one indexed element :-(
    ; ftmorf creates a table, but the morphed fts must be numbered not named
    ; (because the table-numbers must be stored in a table).
    ; kPatch init giPatch_$CHA
	; if kPatch < 8 then
      ftmorf    giPatch_$CHA+giPatch_$CHA+gkMod_$CHA, 99, $CHA
    ; endif

    ; Pan
    $GetController(10'gkPan_$CHA'0'1)
    gkPan_$CHA  portk   gkPan_$CHA, kPortTime
    gkLPan      = sqrt(1.0-gkPan_$CHA)
    gkRPan      = sqrt(gkPan_$CHA)

    ; The normal MIDI-Envelope
    midicontrolchange  73, giAttack_$CHA, 0.01, 1
    iAttack            = 10 * giAttack_$CHA * giAttack_$CHA
    midicontrolchange  75, giDecay_$CHA,  0.001, 1
    iDecay             = 2 * giDecay_$CHA
    midicontrolchange  72, giRelease_$CHA, 0.001, 1
    iRelease           = 10 * giRelease_$CHA
    ; prints "iAttack=%g iDecay=%g iRelease=%g%n", iAttack,iDecay,iRelease
    if giDecay_$CHA > 0.99 then   ; sustained...
      kEnv     linsegr  0, iAttack,1, iRelease,0
    else
      kEnv     linsegr  0, iAttack,1, iDecay,0.707, iDecay,0.5, iDecay,0.353, \
               iDecay,0.250, iDecay,0.177, iDecay,0.125, iDecay,0.088, \
               iDecay,0.062, iDecay,0.044, iDecay,0.031, iDecay,0.022, \
               iDecay,0.015, iDecay,0.011, iDecay,0.007, iRelease,0
    endif
    aEnv     interp   kEnv ; create a smoothed a-rate envelope

    ; Low-pass Filter
    $GetController(74'gkFilFre_$CHA'0'127)
    $GetController(71'gkFilQua_$CHA'0'1)
    gkFilFre_$CHA    portk   gkFilFre_$CHA, kPortTime
    gkFilQua_$CHA    portk   gkFilQua_$CHA, kPortTime
    kFilFre =  30 * cpsmidinn(gkFilFre_$CHA) ^ 0.7
    kFilQua =  0.5 + 1.5 * (gkFilQua_$CHA + gkFilQua_$CHA*gkFilQua_$CHA)

    ; Vibrato
    $GetController(76'gkVibRat_$CHA'4'8)  ; Hz
    $GetController(77'gkVibDep_$CHA'0'1)  ; max nearly 2 semitones
    midicontrolchange  78, giVibDel_$CHA, 0, 1
    iVibDel     =     0.1 * 100^giVibDel_$CHA   ; sec
    kVibEnv     linseg  0, 0.7*iVibDel, 0, 0.6*iVibDel, 0.1, 1, 0.1
    ; kVib      vibr    gkVibDep_$CHA, gkVibRat_$CHA, giSine ; too irregular
    kVib        oscil   gkVibDep_$CHA*gkVibDep_$CHA, gkVibRat_$CHA, 11

    ; Portamento
    midicontrolchange   5, giPorTim_$CHA, 0,2   ; sec
    midicontrolchange  65, giPortOn_$CHA, 0,127
    midicontrolchange  84, giPorMod_$CHA, 0,127 ; portamento start-note 0-127
    if giPortOn_$CHA > 63.5 then
      if giPreOct_$CHA < 1 then
        giPreOct_$CHA = iOct
      endif
      if giPorMod_$CHA > 1.5 then
        kPorEnv linseg  octmidinn(giPorMod_$CHA), giPorTim_$CHA, iOct, 1, iOct
      else
        kPorEnv linseg  giPreOct_$CHA, giPorTim_$CHA, iOct, 1, iOct
      endif
    else
      kPorEnv   = iOct
    endif
    giPreOct_$CHA = iOct

    ; Loop   NON-STANDARD
    midicontrolchange  44, gkLooTyp_$CHA, 0,127 ; Loop type echo/loop/rabbit/MT
    midicontrolchange  45, gkLooVol_$CHA,  0, 1 ; Loop Volume
    midicontrolchange  46, gkLooDel1_$CHA, 0, 4 ; Loop Delay 1
    midicontrolchange  47, gkLooDel2_$CHA, 0, 4 ; Loop Delay 2
    kchanged      changed gkLooTyp_$CHA, gkLooDel1_$CHA
    if kchanged > 0.5 then   ; could also test if gkLooTyp_$CHA>1.4
      gkLooFlg_$CHA = 1   ; in rabbit or mt2, switches between 1 and -1
      gkLooNum_$CHA = 0   ; counter, used as arg for rabbit or mt2 opcodes
      gkLooTim_$CHA times ; timer, remembers the end of the current delay
      gkLooTim_$CHA = gkLooTim_$CHA + gkLooDel1_$CHA
    endif

    ; Overdrive Distortion   NON-STANDARD
    $GetController(87'gkOvrDrv_$CHA'0.9'4)   ; off, plus 1..4

    ; Ring Modulation   NON-STANDARD
    midicontrolchange  88, gkRinMod_$CHA
    gkRinMod_$CHA = int(0.5 + gkRinMod_$CHA)

    ; Reverb
    midicontrolchange  91, gkRevDep_$CHA, 0, 0.6

    ; Tremolo (use cc90 to mean Tremolo Rate)
    $GetController(90'gkTreRat_$CHA'2'8)  ; Hz NON-STANDARD
    $GetController(92'gkTreDep_$CHA'0'1)
    kTre        oscil   gkTreDep_$CHA, gkTreRat_$CHA, 11
    kTre        = kTre + 1

    ; Phaser
    midicontrolchange  95, gkPhaDep_$CHA, 0, 0.5
    gkPhaDep_$CHA portk   gkPhaDep_$CHA, kPortTime ; smoothed using portk

    ; Pitch Bend
    ;midicontrolchange 101, kDummy          ; Registered-parameter MSB
    ;midicontrolchange 100, gkRegPar_$CHA   ; Registered-parameter LSB
    midicontrolchange   6, kData           ; Data
    ;printks "gkRegPar_$CHA=%g%n", 3, gkRegPar_$CHA
    ;if gkRegPar_$CHA < 1 then   ; means PitchBendRange
      gkBenRan_$CHA = kData
    ;endif
    $GetPitchBend(kBend)
    kBend       = (round(gkBenRan_$CHA) * kBend) / 12
    gkBend_$CHA portk   kBend, kPortTime ; smoothed using portk

    aAmp        init iAmp         ; a-rate otherwise the product is wrong ?!
    aAmp2       = aAmp*aVol*aEnv*kTre
    aFre        = cpsoct(kPorEnv+kBend) * (1.0 + kVibEnv*kVib)
    if giPatch_$CHA == 0 then     ; Sine modulating to Triangle
      aSig      oscil   0.98*aAmp2, aFre, $CHA ; play the morfed table
    elseif giPatch_$CHA == 1 then ; Triangle modulating to Spiky triangle
      aSig      oscil   0.48*aAmp2, aFre, $CHA
    elseif giPatch_$CHA == 2 then ; 65% Square modulating to 100% Square
      aSig      oscil   0.44*aAmp2, aFre, $CHA
    elseif giPatch_$CHA == 3 then ; Square modulating to Sawtooth
      aSig      oscil   0.60*aAmp2, aFre, $CHA
    elseif giPatch_$CHA == 4 then ; Sawtooth modulating to Fractalised Sawtooth
      aSig      oscil   0.44*aAmp2, aFre, $CHA
    elseif giPatch_$CHA == 5 then ; 75% Sawtooth modulating to Fractalised 75%
      aSig      oscil   0.60*aAmp2, aFre, $CHA
    elseif giPatch_$CHA == 6 then ; Sine modulating to 9 harmonics of a Square
      aSig      oscil   0.90*aAmp2, aFre, $CHA
    elseif giPatch_$CHA == 7 then ; Fat Sine modulating to Thin Sine
      aSig      oscil   0.56*aAmp2, aFre, $CHA
    elseif giPatch_$CHA == 8 then ; Buzz
      aSig      gbuzz   0.4*aAmp2, aFre, 70, 0, 0.45+0.4*gkMod_$CHA, 11
    elseif giPatch_$CHA == 9 then ; Sawtooth modulating to Square
      aSig      oscil   0.54*aAmp2, aFre, $CHA
    elseif giPatch_$CHA == 10 then ; RectSin modulating to more harmonics
      aSig      oscil   0.64*aAmp2, aFre, $CHA
    elseif giPatch_$CHA == 11 then
      aSig1     oscil   0.64*aAmp2, aFre, 11
      aSig2     oscil   0.64*aAmp2, 2.0*aFre, 11
      aSig4     oscil   0.64*aAmp2, 4.0*aFre, 11
      aSig      = 0.80*aSig1 + 0.10*aSig2 + 0.20*aSig4
    elseif giPatch_$CHA == 12 then
      kFre        = cpsoct(kPorEnv+kBend) * (1.0 + kVibEnv*kVib)
      aSig      pinkish 1
      aSig      lowpass2  aSig, kFre, 6
      aSig      = 0.70*aAmp2*aSig
    elseif giPatch_$CHA == 13 then
      aSig      oscili   0.68*aAmp2, aFre, giThinSin3
    elseif giPatch_$CHA == 14 then
      aSig      oscili   0.60*aAmp2, aFre, giThinSin5
    elseif giPatch_$CHA == 15 then
      aSig      oscili   0.64*aAmp2, aFre, giRectSin
    elseif giPatch_$CHA == 20 then   ; the Fractal Patches...
      aSig      oscili   0.56*aAmp2, aFre/16.0, giMT_1fr
    elseif giPatch_$CHA == 21 then
      aSig      oscili   0.56*aAmp2, aFre/16.0, giMT_2fr
    elseif giPatch_$CHA == 22 then
      aSig      oscili   0.56*aAmp2, aFre/16.4, giRab2fr
    elseif giPatch_$CHA == 23 then
      aSig      oscili   0.64*aAmp2, aFre/1217.75, giRabTri
    elseif giPatch_$CHA == 24 then
      aSig      oscil    0.392*aAmp2, aFre/1217.75, giRabSqu
    elseif giPatch_$CHA == 25 then
      aSig      oscili   0.56*aAmp2, aFre/1336.25, giMT2Squ
    elseif giPatch_$CHA == 26 then
      aSig      oscil    0.48*aAmp2, aFre/1336.25, giMT2Squ
      ; aSig      oscil    0.48*aAmp2, aFre/1493, giMT2Squ
    elseif giPatch_$CHA == 91 then
      aL, aR    ins
      aSig      = 0.5 * (aL+aR)
    else  ; default
      aSig      oscil    0.15*aAmp2, aFre, giTria
    endif
    if kFilFre < 20000 then
      aSig        lowpass2  aSig, kFilFre, kFilQua
    endif
    gaSig_$CHA  = gaSig_$CHA + aSig ; Channel total. The rest is in PostProcess
  endif
#

#define LfoChannel(CHA)  #
  ; p1 is channel-number 1-16, p2 is time, p3 (duration) is -1 meaning Held
  if p4 == 1 then  ; initialisation, create vars to midiprogramchange later
    iOct           = 5.0   ; midi pitch in Csound's 'oct' format
    iAmp           = 100   ; note-on event velocity parameter
    giPatch_$CHA   = 100   ; default Sine
    gkLatestNote_$CHA = 0  ; fudge to get Mono behaviour
    p4             = 0     ; get rid of it, it can be persistent :-)
  else
    midinoteonoct  iOct, iAmp
    iFre           = cpsoct(iOct) / 256
    midiprogramchange giPatch_$CHA
    ; prints "LfoCha $CHA p1=%g giPatch_$CHA=%g iFre=%g%n",p1,giPatch_$CHA,iFre
    ; compare gkLatestNote_$CHA and kThisNote to imitate Mono operation
    kThisNote      init 0
    if kThisNote < 0.5 then
      gkLatestNote_$CHA = gkLatestNote_$CHA + 1
      kThisNote    = gkLatestNote_$CHA
    endif
    if kThisNote == gkLatestNote_$CHA then  ; enforce Monophonic behaviour
      if giPatch_$CHA == 100 then
        gkLFO_$CHA   oscil   1, iFre, 11
      elseif giPatch_$CHA == 101 then
        gkLFO_$CHA   oscil   1, iFre, giTria
      elseif giPatch_$CHA == 102 then
        gkLFO_$CHA   oscil   1, iFre, giSqua65
      elseif giPatch_$CHA == 103 then
        gkLFO_$CHA   oscil   1, iFre, giSqua90
      elseif giPatch_$CHA == 104 then
        gkLFO_$CHA   oscil   1, iFre, giSqua
      elseif giPatch_$CHA == 105 then
        gkLFO_$CHA   oscil   1, iFre, giSawup
      elseif giPatch_$CHA == 106 then
        gkLFO_$CHA   oscil   1, iFre, giSaw90up
      elseif giPatch_$CHA == 107 then
        gkLFO_$CHA   oscil   1, iFre, giSaw75up
      elseif giPatch_$CHA == 108 then
        gkLFO_$CHA   oscil   1, iFre, giTria
      elseif giPatch_$CHA == 100 then
        gkLFO_$CHA   oscil   1, iFre, giSaw75do
      elseif giPatch_$CHA == 110 then
        gkLFO_$CHA   oscil   1, iFre, giSaw90do
      elseif giPatch_$CHA == 111 then
        gkLFO_$CHA   oscil   1, iFre, giSawdo
      else
        gkLFO_$CHA   oscil   1, iFre, 12 ; default triangle
      endif
    endif
  endif
#

instr 1    ; channel 0
  $AudioChannel(1)
endin
instr 2    ; channel 1
  $AudioChannel(2)
endin
instr 3    ; channel 2
  $AudioChannel(3)
endin
instr 4    ; channel 3
  $AudioChannel(4)
endin
instr 5    ; channel 4
  $AudioChannel(5)
endin
instr 6    ; channel 5
  $AudioChannel(6)
endin
instr 7    ; channel 6
  $AudioChannel(7)
endin
instr 8    ; channel 7
  $AudioChannel(8)
endin
instr 9    ; channel 8
  $AudioChannel(9)
endin

instr 10    ; channel 9 is the Percussion, as in GM
  kkey      init 0
  kvelocity init 0
  midinoteonkey kkey, kvelocity
  kAmp      = kvelocity / 127

  kcontroller      init 0
  kcontrollervalue init 0
  midicontrolchange kcontroller, kcontrollervalue
  kVol      init 0.8
  kLPan     = 0.707
  kRPan     = 0.707
  if kcontroller == 7 then
    kVol    = kcontrollervalue / 127
  elseif kcontroller == 10 then
    kPan    = kcontrollervalue / 127
  endif
  kTrigger    changed  kPan ; if kPan changes, recalculate kLPan, kRPan
  if kTrigger == 1 then
    kLPan   = sqrt(1.0-kPan)
    kRPan   = sqrt(kPan)
  endif
  ; printf "kLPan=%g kRPan=%g%n", kPan, kLPan, kRPan

  if kkey == 33 then ; metronome beat
    kEnv    expseg .6, 0.04, .02, 0.2, .000005
    aSig    oscili  kAmp*kVol*kEnv, 1100, 11
  elseif kkey == 34 then ; metronome barline
    kEnv    expseg .4, 0.15, .02, 0.6, .000005
    aSig    oscili  kAmp*kVol*kEnv, 3000, 11
  elseif kkey == 35 then ; Acoustic Bass Drum
    kEnv    expseg 1, 0.1, .15, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 50, 11
  elseif kkey == 36 then ; Bass Drum 1
    kEnv    expseg 1, 0.1, .2, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 65, 11
  elseif kkey == 37 then ; Side Stick
    kEnv    expseg 1.1, 0.05, .001, 0.5, .00001
    aSig    oscili  kAmp*kVol*kEnv, 900, giTria
  elseif kkey == 38 then ; Acoustic Snare
    kEnv    expseg 1, 0.1, .2, 0.2, .002
    aSig    noise  kAmp*kVol*kEnv, 0.0
  elseif kkey == 40 then ; Electric Snare
    kEnv    expseg 1, 0.1, .2, 1, .002
    aSig    noise  kAmp*kVol*kEnv, 0.7
  elseif kkey == 41 then ; Tom
    kEnv    expseg 1, 0.1, .1, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 105, 11
  elseif kkey == 43 then ; Tom
    kEnv    expseg 1, 0.1, .1, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 122, 11
  elseif kkey == 45 then ; Tom
    kEnv    expseg 1, 0.1, .1, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 142, 11
  elseif kkey == 48 then ; Tom
    kEnv    expseg 1, 0.1, .1, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 165, 11
  elseif kkey == 50 then ; Tom
    kEnv    expseg 1, 0.1, .1, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 190, 11
  elseif kkey == 60 then ; High Bongo
    kEnv    expseg .5, 0.1, .02, 1, .000005
    aSig    oscili  kAmp*kVol*kEnv, 410, 11
  elseif kkey == 61 then ; Low Bongo
    kEnv    expseg .5, 0.1, .05, 1, .00005
    aSig    oscili  kAmp*kVol*kEnv, 300, 11
  elseif kkey == 62 then ; Mute High Conga
    kEnv    expseg 1, 0.1, .002, 0.5, .00001
    aSig    oscili  kAmp*kVol*kEnv, 215, 11
  elseif kkey == 63 then ; Open High Conga
    kEnv    expseg 1, 0.1, .10, 1, .00001
    aSig    oscili  kAmp*kVol*kEnv, 205, 11
  elseif kkey == 64 then ; Low Conga
    kEnv    expseg 1, 0.1, .12, 1, .0001
    aSig    oscili  kAmp*kVol*kEnv, 150, 11
  elseif kkey == 81 then ; Open Triangle
    kEnv    expsegr 0.3, 0.01, .15, 10, .002, 10, .0001
    aSig    oscili  kAmp*kVol*kEnv, 4000, 11
  endif
  outs      kLPan*aSig, kRPan*aSig
endin

instr 11   ; channel 10
  $LfoChannel(11)
endin
instr 12   ; channel 11
  $LfoChannel(12)
endin
instr 13   ; channel 12
  $LfoChannel(13)
endin
instr 14   ; channel 13
  $LfoChannel(14)
endin
instr 15   ; channel 14
  $LfoChannel(15)
endin
instr 16   ; channel 15
  $LfoChannel(16)
endin

#define PostProcess(CHA) #

  ; Ring Modulator must be here so all channels have set their gaSig_* already
  if gkRinMod_$CHA < 8.5 then
    if gkRinMod_$CHA < 0.5 then ; Ring-Modulate with Channel 0
      gaSig_$CHA = gaSig_$CHA * gaSig_1
      gaSig_1 = 0
    elseif gkRinMod_$CHA < 1.5 then ; with Channel 1
      gaSig_$CHA = gaSig_$CHA * gaSig_2
      gaSig_2 = 0
    elseif gkRinMod_$CHA < 2.5 then
      gaSig_$CHA = gaSig_$CHA * gaSig_3
      gaSig_3 = 0
    elseif gkRinMod_$CHA < 3.5 then
      gaSig_$CHA = gaSig_$CHA * gaSig_4
      gaSig_4 = 0
    elseif gkRinMod_$CHA < 4.5 then
      gaSig_$CHA = gaSig_$CHA * gaSig_5
      gaSig_5 = 0
    elseif gkRinMod_$CHA == 5 then
      gaSig_$CHA = gaSig_$CHA * gaSig_6
      gaSig_6 = 0
    elseif gkRinMod_$CHA < 6.5 then
      gaSig_$CHA = gaSig_$CHA * gaSig_7
      gaSig_7 = 0
    elseif gkRinMod_$CHA < 7.5 then
      gaSig_$CHA = gaSig_$CHA * gaSig_8
      gaSig_8 = 0
    else
      gaSig_$CHA = gaSig_$CHA * gaSig_9
      gaSig_9 = 0
    endif
    gaSig_$CHA = gaSig_$CHA * 2
  endif

  ; gkOvrDrv_$CHA
  ; /home/ports/swh-plugins-0.4.15/foverdrive_1196.c
  ; drivem1 = drive - 1.0 ; float fx = fabs(x);
  ; output   x*(fx + drive)/(x*x + drivem1*fx + 1.0);
  if gkOvrDrv_$CHA > 0.95 then
    kDm1 = gkOvrDrv_$CHA - 1
    aAbs = abs(gaSig_$CHA)
    gaSig_$CHA = gaSig_$CHA * ( aAbs + gkOvrDrv_$CHA ) \
      / ( gaSig_$CHA*gaSig_$CHA + kDm1*aAbs + 1.0 )
  endif

  ; become stereo
  aSigL = 0.25 * gkLPan * gaSig_$CHA
  aSigR = 0.25 * gkRPan * gaSig_$CHA

  ; Phaser
  if gkPhaDep_$CHA > 0.03 then  ; BUG in phaser2 if >7 phasers run at once!
	kPhaLin  oscil 1, 0.15, giPhaL ; stereoise with out of phase phasers
    aPhaL    phaser2 aSigL, kPhaLin*2000, .7, 8, 0.9, 2, .33
    aSigL    = aSigL * (1-gkPhaDep_$CHA) + aPhaL * gkPhaDep_$CHA
	kPhaRin  oscil 1, 0.19, giPhaR ; with out of phase phasers stereoise
    aPhaR    phaser2 aSigR, kPhaRin*2000, .7, 8, 0.9, 2, .33
    aSigR    = aSigR * (1-gkPhaDep_$CHA) + aPhaR * gkPhaDep_$CHA
  endif

  ; delay needs idlt but inst 99 only initialises once
  ; but it _must_ go in instr 99! the MIDI-note finishes after release.
  ; so use delayr and delayw at 5sec, then tap in with deltap ...
  ; prints "gkLooVol_$CHA=%g gkLooDel1_$CHA=%g%n",gkLooVol_$CHA,gkLooDel1_$CHA
  ; Changes in cc45 go unnoticed until a new note is struck in the channel :-)
  if gkLooVol_$CHA > 0.1 then     ; Loop Volume ?
    if gkLooDel1_$CHA > .02 then   ; Loop Delay ?
      if gkLooTyp_$CHA == 0 then ; type=Single-Echo
        adumpL   delayr  4.1
        aLooL    deltap gkLooDel1_$CHA
        delayw   aSigL
        aSigL    = aSigL + aLooL*gkLooVol_$CHA
        adumpR   delayr  4.1
        aLooR    deltap gkLooDel1_$CHA
        delayw   aSigR
        aSigR    = aSigR + aLooR*gkLooVol_$CHA
      elseif gkLooTyp_$CHA == 1 then ; type=Loop
        adumpL   delayr  4.1
        aLooL    deltap gkLooDel1_$CHA
        aSigL    = aSigL + aLooL*gkLooVol_$CHA
        delayw   aSigL
        adumpR   delayr  4.1
        aLooR    deltap gkLooDel1_$CHA
        aSigR    = aSigR + aLooR*gkLooVol_$CHA
        delayw   aSigR
      elseif gkLooTyp_$CHA > 1.5 then ; type=Rabbit or Morse-Thue
        ktime  times
        if ktime > gkLooTim_$CHA then
          gkLooNum_$CHA =   gkLooNum_$CHA + 1
          if gkLooTyp_$CHA > 2.5 then ; type=Morse-Thue
            gkLooFlg_$CHA table gkLooNum_$CHA, 91, 0, 0, 1
          else  ; type=Rabbit
            gkLooFlg_$CHA table gkLooNum_$CHA, 90, 0, 0, 1
          endif
          gkLooTim_$CHA =   ktime
          if gkLooFlg_$CHA > 0 then
            gkLooTim_$CHA = gkLooTim_$CHA + gkLooDel1_$CHA
          else
            gkLooTim_$CHA = gkLooTim_$CHA + gkLooDel2_$CHA
          endif
        endif
        if gkLooFlg_$CHA > 0 then
          adumpL   delayr  4.1
          aLooL    deltap gkLooDel1_$CHA
          aSigL    = aSigL + aLooL*gkLooVol_$CHA
          delayw   aSigL
          adumpR   delayr  4.1
          aLooR    deltap gkLooDel1_$CHA
          aSigR    = aSigR + aLooR*gkLooVol_$CHA
          delayw   aSigR
        else
          adumpL   delayr  4.1
          aLooL    deltap gkLooDel2_$CHA
          aSigL    = aSigL + aLooL*gkLooVol_$CHA
          delayw   aSigL
          adumpR   delayr  4.1
          aLooR    deltap gkLooDel2_$CHA
          aSigR    = aSigR + aLooR*gkLooVol_$CHA
          delayw   aSigR
        endif
      endif
    endif
  endif

  ; Reverb
  outs     aSigL*(1-gkRevDep_$CHA), aSigR*(1-gkRevDep_$CHA) ; the dry signal
  if gkRevDep_$CHA > 0.05 then
    aRevL      reverb aSigL, 3         ; could make Time depend on Depth ?
    aRevR      reverb aSigR, 3
    outs aRevR*gkRevDep_$CHA, aRevL*gkRevDep_$CHA ; pan opposite to dry signal
  endif
  gaSig_$CHA   = 0       ; empty the receiver for the next pass
#

instr 99   ; (highest instr number executed last, see reverb.csd)
  $PostProcess(1)
  $PostProcess(2)
  $PostProcess(3)
  $PostProcess(4)
  $PostProcess(5)
  $PostProcess(6)
  $PostProcess(7)
  $PostProcess(8)
  $PostProcess(9)
endin

</CsInstruments>
<CsScore>

; Table space is allocated in primary memory, along with instrument
; data space. The maximum table number used to be 200. This has been
; changed to be limited by memory only. (Currently there is an internal
; soft limit of 300, this is automatically extended as required.)
f 1 0 1024 10 1 ; morphed table for instr 1 (channel 0)
f 2 0 1024 10 1 ; morphed table for instr 2
f 3 0 1024 10 1 ; morphed table for instr 3
f 4 0 1024 10 1 ; morphed table for instr 4
f 5 0 1024 10 1 ; morphed table for instr 5
f 6 0 1024 10 1 ; morphed table for instr 6
f 7 0 1024 10 1 ; morphed table for instr 7
f 8 0 1024 10 1 ; morphed table for instr 8
f 9 0 1024 10 1 ; morphed table for instr 9 (channel 8)
f 11 0 1024 10 1   ; Sine wave
f 12 0 1024  7 0 256 1  512 -1 256  0 ; Triangle wave
f 13 0 1024  7 0  90 1  332  1 180 -1 332 -1 90 0  ; 65% Square wave
f 14 0 1024  7 0  26 1  460  1  52 -1 460 -1 26 0  ; 90% Square wave
f 15 0 1024  7 0   1 1  510  1   2 -1 510 -1  1 0  ; 100% Square wave
f 16 0 1024  7 0  86 0  170  1 170 0 172 0 170 -1 170 0 86 0 ; Spiky Triangle
f 17 0 1024  7 0 \
 1  1  56  1  1  0.5  56  0.5  1  1  56  1 \
 1  0  56  0  1  0.5  55  0.5  1  0  56  0 \
 1  1  56  1  1  0.5  56  0.5  1  1  56  1 \
 1 -1  56 -1  1 -0.5  56 -0.5  1 -1  56 -1 \
 1  0  56  0  1 -0.5  55 -0.5  1  0  56  0 \
 1 -1  56 -1  1 -0.5  56 -0.5  1 -1  56 -1         ; Castellated square
f 18 0 1024  7 0  1 1  1022 -1  1 0 ; Sawtooth
; Fractalised Sawtooth
f 19 0 1024  7 0  1 1  245 0  1 0.5  255 -0.5 1 0.5 255 -0.5 1 0 254 -1  1 0
f 20 0 1024  7 0  64 1  896 -1  64 0 ; 75% Sawtooth
; Fractalised 75% Sawtooth
f 21 0 1024  7 0  64 1  192 0 32 0.5 192 -0.5 64 0.5 192 -0.5 32 0 192 -1 64 0
f 22 0 1024 10 0.75 0 0.25 0 .15 0 .11 0 .08 ; 9 harmonics of a Square
f 23 0 1024 8 0  64 .6186  64 .8409  64 .9612 \
 64 1  64 .9612  64 .8409  64 .6186  64 0  64 -.6186  64 -.8409  \
 64 -.9612  64 -1  64 -.9612  64 -.8409  64 -.6186  64 0 ; Fat (sqrt of) sine
;giThinSin3 ftgen    0, 0, 1024, 10, 0.75, 0, -0.25
f 24 0 1024 10 0.6522 0 -0.2174 0 .1304 ; Thin Sine
f 25 0 1024 8 -1 64 -0.6936  64 -0.3989  64 -0.1273  \
 64 .1107  64 .3061  64 .4512  64 .5406  64 .5708  64 .5406  64 .4512  \
 64 .3061  64 .1107  64 -.1273  64 -.3989  64 -.6936  64 -1 ; RectSine
f 26 0 1024 7 \
-1 16 -0.832808 16 -0.672529 16 -0.525566 16 \
-0.397343 16 -0.291916 16 -0.211696 16 -0.157307 16 \
-0.127581 16 -0.11971 16 -0.129512 16 -0.109437 16 \
-0.0154567 16 0.0632491 16 0.12264 16 0.159844 16 \
0.17344 16 0.219845 16 0.242497 16 0.242674 16 \
0.223103 16 0.187683 16 0.225454 16 0.292245 16 \
0.340371 16 0.365515 16 0.364736 16 0.336738 16 \
0.282018 16 0.202866 16 0.103218 16 -0.0116137 16 \
-0.135337 16 -0.0116137 16 0.103218 16 0.202866 16 \
0.282018 16 0.336738 16 0.364736 16 0.365515 16 \
0.340371 16 0.292245 16 0.225454 16 0.187683 16 \
0.223103 16 0.242674 16 0.242497 16 0.219845 16 \
0.17344 16 0.159844 16 0.12264 16 0.0632491 16 \
-0.0154567 16 -0.109437 16 -0.129512 16 -0.11971 16 \
-0.127581 16 -0.157307 16 -0.211696 16 -0.291916 16 \
-0.397343 16 -0.525566 16 -0.672529 16 -0.832808 16 -1 ; RectSine + harmonics

; f90 an f91 are rabbit and mt2 arrays
; f 90 0 8 2  1 -1  1 1 -1 1 -1  1
; f 91 0 8 2  1 -1 -1 1 -1 1  1 -1
f 90 0 8192 -23 "RabSqu.txt"
f 91 0 8192 -23 "MT2Squ.txt"

; f99 is the morphing partners, 11<->12, 12<->16  etc
f 99 0   32 -2  11 12  12 16  13 15  15 17  18 19  20 21  11 22  23 24 \
                11 12  18 15  25 26  11 12  11 12  11 12  11 12  11 12
; giSqua65   ftgen    0, 0, 1024, 7, 0, 90, 1, 332, 1, 180, -1, 332, -1, 90, 0
; giSqua90   ftgen    0, 0, 1024, 7, 0, 26, 1, 460, 1,  52, -1, 460, -1, 26, 0
; giSqua     ftgen    0, 0, 1024, 7, 0,  1, 1, 510, 1,   2, -1, 510, -1,  1, 0

i 1 0 0 1 ; inititialise the instruments
i 2 0 0 1
i 3 0 0 1
i 4 0 0 1
i 5 0 0 1
i 6 0 0 1
i 7 0 0 1
i 8 0 0 1
i 9 0 0 1
i 11 0 0 1
i 12 0 0 1
i 13 0 0 1
i 14 0 0 1
i 15 0 0 1
i 16 0 0 1

i 99 0 3600  ; keep the add-reverb-and-output instrument running

f 0 3600

e
</CsScore>
</CsoundSynthesizer>

/*

my $Version       = '1.22';
my $VersionDate   = '14jan2013';

=pod

=head1 NAME

pjbsynth.csd - a Csound script for a low-tech old-fashioned synth

=head1 SYNOPSIS

 # after a full install:
 pjbsynth -M 99         # to get a list of your MIDI-device numbers
 pjbsynth -M 6          # connect your MIDI-device 6 to the synth
 pjbsynth               # connects default MIDI-device 0 to the synth
 pjbsynth -O null       # doesn't log to the screen
 pjbsynth -T -F in.mid  # plays the MIDI-file in.mid
 pjbsynth -i in.wav     # in.wav will appear as patch 91
 pjbsynth -h            # read the manual

 # after a minimal install, with copies of pjbsynth.csd
 # and of the *.txt files in your current directory:
 csound -M 99 pjbsynth.csd    # get a list of your MIDI-device numbers
 csound -M 6 pjbsynth.csd     # connect your MIDI-device 6 to the synth
 csound pjbsynth.csd          # connects MIDI-device 0 to the synth
 csound -O null pjbsynth.csd  # doesn't log to the screen
 csound -T -F in.mid pjbsynth.csd   # plays the MIDI-file in.mid
 csound -i in.wav pjbsynth.csd  # in.wav will appear as patch 91
 perldoc pjbsynth.csd           # read the manual

=head1 DESCRIPTION

This I<csound> script takes MIDI input and produces audio output.
It is still at an early release stage, and is likely to change
even in important details like Patches and Controller-numbers.

Channels 0-8 are dedicated to Audio signals.
The basic Patches are simple, old-fashioned waveforms
like sine, triangle, square, and sawtooth,
plus some fractal waveforms, and also live or file audio.

The Channel Controllers for these Audio channels
can be set by the normal methods,
or one or two of them can be driven by a built-in Low-Frequency-Oscillator
by using the non-standard controllers cc20 to cc23, or cc24 to cc27,
or by a built-in Attack-Envelope using cc52 to cc53.
Other non-standard controllers offer also
Ring modulation, Distortion, some unusual types of Loop, etc.

Channel 9 is dedicated to a low-tech percussion-set
using simple waveforms like sine or white noise.
It is roughly modelled on General-MIDI Channel 9,
see http://www.pjb.com.au/muscript/gm.html#perc

Channels 10-15 are dedicated to the Low-Frequency-Oscillators,
where MIDI-note number 60 means not middle-C,
but 1 Cycle-per-second (i.e. eight octaves lower!).

An example I<setup1.mid> is included,
which sets up I<pjbsynth> with a few plausible sounds.

=head1 PATCHES

=over 3

=item B<Audio Patches> for channels 0-8 (Note-number 60 is middle-C)

  0 = Sine wave       (modulating to triangle)
  1 = Triangle wave   (modulating to spiky triangle)
  2 = 65% Square wave (modulating to 100% square wave)
  3 = Square wave     (modulating to castellated-square wave)
  4 = Sawtooth wave   (modulating to fractalised sawtooth)
  5 = 75%-Sawtooth    (modulating to fractalised 75% sawtooth)
  6 = Sine wave       (modulating to 9 harmonics of a square wave)
  7 = Fat Sine wave   (modulating to a 5-harmonic thin sine wave)
  8 = Buzz            (round modulating to buzzy)
  9 = Sawtooth wave   (modulating to square wave)
 10 = Rectified Sine  (modulating to rectified sine with harmonics)
 12 = Fat Sine
 13 = Thin Sine with 3rd harmonic
 14 = Thin Sine with 3rd and 5th harmonics
 15 = Rectified Sine
 20 = Mild Morse-Thue-fractal-frequency-modulated Triangle
 21 = Moderate Morse-Thue-fractal frequency-modulated Triangle
 22 = Moderate Rabbit-fractal frequency-modulated Triangle
 23 = Rabbit-fractal Triangle wave
 24 = Rabbit-fractal Square wave
 25 = Morse-Thue-fractal Triangle wave
 26 = Morse-Thue-fractal Square wave
 90 = Live stereo audio input (currently unimplemented)
 91 = Live audio input mixed down to mono

=item B<LFO Patches> for channels 10-15 (Note-number 60 is 1 cycle-per-second !)

The Low-Frequency-Oscillators live in Channels 10 and above,
and use Patches 100 and above.
Their frequencies are eight octaves lower than the audio Patches,
so that Note number 60 means 1 cycle-per-second.
These two data-items, the Patch and the "Note",
are the only two that Low-Frequency-Oscillators need.

 100 = Sine wave
 101 = Triangle wave
 102 = Square wave 65%
 103 = Square wave 90%
 104 = Square wave
 105 = Sawtooth wave Up
 106 = Intermediate Triangle/Sawtooth wave 90% Up
 107 = Intermediate Triangle/Sawtooth wave 75% Up
 108 = another Triangle wave
 109 = Intermediate Triangle/Sawtooth wave 75% Down
 110 = Intermediate Triangle/Sawtooth wave 90% Down
 111 = Sawtooth wave Down

=back

=head1 STANDARD CONTROLLER-NUMBERS

=over 3

See http://www.pjb.com.au/muscript/gm.html#cc

   1 = Modulation
   5 = Portamento time
   6 = Data Entry MSB
   7 = Channel volume
  10 = Pan
  11 = Expression
  38 = Data Entry LSB
  64 = Sustain pedal
  65 = Portamento on-off
  71 = Filter Q (affects the cc74 filter)
  72 = Release time
  73 = Attack time
  74 = Low-pass Filter frequency
  75 = Decay time
  76 = Vibrato rate
  77 = Vibrato depth
  78 = Vibrato delay
  84 = Portamento from-note
  91 = Reverb depth
  92 = Tremolo depth
  95 = Phaser depth
 100 = Registered Parameter LSB
 101 = Registered Parameter MSB

The Pitch-bend-range is a "Registered Parameter",
and should (but see BUGS below) be adjusted by:

 cc101=0, cc100=0, cc6=0..24 semitones, cc38=0, cc101=127, cc100=127 

=back

=head1 NON-STANDARD CONTROLLER-NUMBERS

cc20-23 connect a controller (cc21) to an LFO (cc20),
and cc24-27 connect another controller (cc25) to an LFO (cc24).
Similarly, cc52-55 connect a controller (cc53)
to an Effect-Envelope launched at the start of each note
(which also affects other notes ongoing in the same Channel).

When being driven by an LFO or an Effect-Envelope in this way,
the controller will ignore its standard MIDI Controller-Change commands.

If in one channel cc21 and cc25 both attempt to connect the same controller
(this would be an error) then the cc20-cc23 specification takes precedence.
But multiple controllers in many channels may be driven by the same LFO
without problem.

 20 = LFO-channel (10..15; default 0=off)
 21 = an audio Channel-Controller that the LFO will control
      (cc21=0 is special-cased to mean Pitch-Bend)
 22 = the minimum value of that audio Channel-Controller
 23 = the maximum value of that audio Channel-Controller

 24 = LFO-channel (10..15; default 0=off)
 25 = another audio Channel-Controller that this new LFO will control
      (cc25=0 is special-cased to mean Pitch-Bend)
 26 = the minimum value of that audio Channel-Controller
 27 = the maximum value of that audio Channel-Controller

 52 = Effect-Envelope Attack-Time (0..127; default 0=off)
 53 = the audio Channel-Controller that this Effect-Envelope will control
      (cc53=0 is special-cased to mean Pitch-Bend)
 54 = the initial value of that audio Channel-Controller
 55 = the  final  value of that audio Channel-Controller

There are several other non-standard CC's:

  14 = Choof (unimplemented)
  15 = Clonk (unimplemented)
  44 = Loop Type (0=Echo (default), 1=Loop, 2=Rabbit, 3=Morse-Thue)
  45 = Loop Volume
  46 = Loop Delay 1 (0..4 sec)
  47 = Loop Delay 2
  87 = Overdrive Distortion (modelled on SWH foverdrive_1196)
  88 = Ring Modulator Channel (usually set to Patch 0)
  90 = Tremolo rate (affects the cc92 tremolo)
 119 = Dump current settings to pjbsynth.state.py (unimplemented)
       will use subinstr, srtcatk, fprintks to print non-default gkCC*
       in either muscript, MIDI-Perl, MIDI.py, MIDI.lua, or .mid format

If cc87=0 then Distortion is switched off.

If cc88=127 then Ring-Modulation is switched off.
If cc88 points to another audio channel (0..8) then
the latter's direct output will be suppressed
and the current channel will be modulated with it.
Usually, that other channel should be a simple wave-form like a sine,
perhaps with portamento.

=head1 INSTALLATION

For a full installation, download the tarball:
http://www.pjb.com.au/midi/free/pjbsynth-1.22.tar.gz ,
unpack it, and, as superuser: I<make install> .
This puts some files in I</usr/local/share/pjbsynth/>
and copies a wrapper script to I</usr/local/bin/pjbsynth>

For a minimal installation, unpack the tarball and
copy I<pjbsynth.csd> and the I<*.txt> files into your working directory.

See the SYNOPSIS for the difference in usage.

You will also need I<csound> version 5 installed.

=head1 CHANGES

 20130117 1.22 fix two bugs reported by Pete Goodeve on csound 5.19
 20110120 1.21 Loop stuff moved to CCs 44-74; Rabbit and MT2 Loops
 20110104 1.20 standard cc71 means filter Q :-) so get rid of cc89
 20101228 1.19 cc85 and cc86 now do Loop, not just Echo
 20101224 1.18 two more modulating patches
 20101220 1.17 cc11 Expression
 20101219 1.16 tarball includes the up-to-date test_score
 20101219 1.15 fixed cc53=0 Envelope-driven Bend bug; other minor fixes
 20101217 1.14 fixed LFO bug, new modulating patches 0..6
 20101211      disentangled cc53=0 (Bend) from the other cc53 settings
 20101210 1.13 stereo phaser, cc52-55 allow attack-Envelope control
 20101202 1.12 Ring modulator, Distortion, Patch 91
 20101127 1.11 first uploaded to www.pjb.com.au
 20101110 1.10 first working version

 TODO:
  Choof CC using perhaps very mildly filtered pink noise
  Clonk CC using very heavily damped sine at perhaps sqrt of note-pitch
  Note=0 guaranteed silent (used to force a reread of Loop parameters)
  MT and Rabbit CC somehow (two delay params), not just boring old Loop
   (won't need Loop, MT and Rabbit simultaneously; how to express this?)
  A filtered pink-noise patch
  Stereo live audio through, patch 90
  State dump
  Give the Reverb an initial delay, somehow increasing with the reverb time

=head1 BUGS

The I<phaser2> function needs a fast CPU,
especially if several phasers are running simultaneously.

The Registered-parameter controllers cc101 and cc100
are handled by I<csound> in some apparently undocumented way.
I<pjbsynth> treats a naked cc6 event as setting the Pitch-bend-range,
but if you send a cc101 and cc100 first (like you're supposed to)
then cc6 produces no effect.

Changes in controller cc45 go unnoticed
until a new note is struck in the channel;
this is inconvenient if a loop is running and you want to fade it out...

=head1 AUTHOR

Peter J Billam   http://www.pjb.com.au/comp/contact.html

=head1 SEE ALSO

 http://www.csounds.com
 http://www.pjb.com.au
 http://www.pjb.com.au/muscript/gm.html
 http://www.pjb.com.au/midi/midikbd.html
 aconnect (1)

=cut

*/
