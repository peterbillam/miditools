<CsoundSynthesizer>
; cha9 works :-)
; but cha0-8 are silent (except occasional clicks 2/sec) :-(
<CsOptions>
; -d -m0 -M0 -iadc -odac -B1024 -b512
; modprobe snd-virmidi snd_index=1   # in /etc/rc.local, then amidi -l
-d -Mhw:1,0 -iadc -odac
; -O null has no effect here :-( it must be invoked at the command-line
</CsOptions>

<CsInstruments>
sr     = 44100
ksmps  = 64
nchnls = 2
0dbfs  = 1

; ares delay asig, idlt

;  ftgen ifn, itime, isize, igen, <GEN routine args...>
giSine     ftgen    0, 0, 1024, 10, 1
giSqua65   ftgen    0, 0, 1024, 7, 0, 90, 1, 332, 1, 180, -1, 332, -1, 90, 0
giSqua90   ftgen    0, 0, 1024, 7, 0, 26, 1, 460, 1,  52, -1, 460, -1, 26, 0
giSqua     ftgen    0, 0, 1024, 7, 0,  1, 1, 510, 1,   2, -1, 510, -1,  1, 0
giTria     ftgen    0, 0, 1024, 7, 0, 256,  1, 512, -1, 256, 0
giSaw75up  ftgen    0, 0, 1024, 7, 0, 384,  1, 256, -1, 384, 0
giSaw75do  ftgen    0, 0, 1024, 7, 0, 384, -1, 256,  1, 384, 0
giSaw90up  ftgen    0, 0, 1024, 7, 0, 461,  1, 102, -1, 461, 0
giSaw90do  ftgen    0, 0, 1024, 7, 0, 461, -1, 102,  1, 461, 0
giSawup    ftgen    0, 0, 1024, 7, -1, 1022, 1,  2,-1
giSawdo    ftgen    0, 0, 1024, 7,  1, 1022,-1,  2, 1
giFatSine  ftgen    0, 0, 1024, 8, 0, 64,.6186, 64,.8409, 64,.9612, \
 64,1, 64,.9612, 64,.8409, 64,.6186, 64,0, 64,-.6186, 64,-.8409, \
 64,-.9612, 64,-1, 64,-.9612, 64,-.8409, 64,-.6186, 64,0 ; sqrt of sin
giRectSin  ftgen    0, 0, 1024, 8, -1, 64,-0.6936, 64,-0.3989, 64,-0.1273, \
 64,.1107, 64,.3061, 64,.4512, 64,.5406, 64,.5708, 64,.5406, 64,.4512, \
 64,.3061, 64,.1107, 64,-.1273, 64,-.3989, 64,-.6936, 64,-1 ; rect sin
giThinSin3 ftgen    0, 0, 1024, 10, 0.75, 0, -0.25
giThinSin5 ftgen    0, 0, 1024, 10, 0.6522, 0, -0.2174, 0, .1304
giSqua5    ftgen    0, 0, 1024, 10, 0.75, 0, 0.25, 0, .14
giSqua7    ftgen    0, 0, 1024, 10, 0.75, 0, 0.25, 0, .15, 0, .1

pgmassign 0,0
massign 1,1
massign 2,2
massign 3,3
massign 4,4
massign 5,5
massign 6,6
massign 7,7
massign 8,8
massign 9,9
massign 10,10
massign 11,11
massign 12,12
massign 13,13
massign 14,14
massign 15,15
massign 16,16

; mididefault 42, pN only has access to p4-p9; 6 variables.
; but there are 127 controllers, plus patch, bend etc :-(
; Hmm... could invoke the instruments in an initialisation-mode,
; right at the beginning of CsScore with p4 set somehow; then they
; could set all their iPatch kPan etc to initial values. These would
; then be in existence, allowing midicontrolchange 10, kPan statements.
; Disadvantage: one extra level of indentation, no big deal.
; But would initialisation be finished before the first midi event arrives?
; Yes, in practice; but there's a big unreliable-persistence-problem :-(

; doc: "initc7 can be used together with both midic7 and ctrl7 opcodes
;  for initializing the first controller's value."
; but it also works with midicontrolchange ....
#define InitMidiControllers(CHA)  #
  initc7  $CHA,  1, 0    ; Modulation
  initc7  $CHA,  7, 0.93 ; Channel-volume
  initc7  $CHA, 10, 0.5  ; Pan
  initc7  $CHA, 11, 0.93 ; Expression
  initc7  $CHA, 72, 0.02 ; Release-time
  initc7  $CHA, 73, 0.02 ; Attack-time
  initc7  $CHA, 75, 1.0  ; Decay-time
  initc7  $CHA, 76, 0.5  ; Vibrato-rate
  initc7  $CHA, 77, 0    ; Vibrato-depth
  initc7  $CHA, 78, 0.5  ; Vibrato-delay
  initc7  $CHA, 87, 0    ; Overdrive Distortion   NON-STANDARD
  initc7  $CHA, 88, 1.0  ; Ring Modulator Channel NON-STANDARD
  initc7  $CHA, 90, 0.497; Tremolo-rate NON-STANDARD
  initc7  $CHA, 92, 0.0  ; Tremolo-depth
#
$InitMidiControllers(1)
$InitMidiControllers(2)
$InitMidiControllers(3)
$InitMidiControllers(4)
$InitMidiControllers(5)
$InitMidiControllers(6)
$InitMidiControllers(7)
$InitMidiControllers(8)
$InitMidiControllers(9)
$InitMidiControllers(11)
$InitMidiControllers(12)
$InitMidiControllers(13)
$InitMidiControllers(13)
$InitMidiControllers(14)
$InitMidiControllers(15)
$InitMidiControllers(16)

; for each channel, need a TVF envelope (p58) and a TVA envelope (p60)
; TVF is L0 T1 L1 T2 L2 T3 L3 T4 L4
; TVA is    T1 L1 T2 L2 T3 L3 T4     and L0 and L4 are zero

#define GetController(CC'VAR'MIN'MAX) #
  ; midicontrolchange doesn't work in an if, so fetch anyway
  midicontrolchange $CC, $VAR, 0, 127
  $VAR = $MIN + $VAR * ($MAX-$MIN) / 128   ; scale 0..127 to MIN..MAX
#

#define GetPitchBend(VAR) #
  ; midipitchbend xpitchbend [, ilow] [, ihigh]
  ; ilow  (optional) -- optional low value after rescaling, defaults to 0.
  ; ihigh (optional) -- optional high value after rescaling, defaults to 127
  ; in fact seems to default to -1..+1
  ; midipitchbend $VAR, 0.88, 1.12  doesn't work, so just return -1..+1
  midipitchbend $VAR
#

#define AudioChannel(CHA)  #
  ; p1 is channel-number 1-16, p2 is time, p3 (duration) is -1 meaning Held
  ; prints "p4=%g%n", p4
  if p4 == 1 then  ; initialisation, create vars to midiprogramchange later
    ; BIG PROBLEM... Persistence of vars is not guaranteed.  If a second
    ; note starts on the same instr before the first one has finished,
    ; then it seems to get a new instance, in new memory,  which does
    ; not include copies of the values of the variables :-( That's a BUG.
    ; FALSE REMEDY... init can initialise svars as well as kvars and avars
    ; but it reinitialises them every note :-(
    ; KLUDGE :-) could create global variables to do the persistence ?
    ; KLUDGE PROBLEM... it only works for gi variables, in spite of kr.html
    ; KLUDGE SOLUTION :-) it works for gk too iff the RHS only contains k's
    iOct           = 5.0   ; midi pitch in Csound's 'oct' format
    iAmp           = 100   ; note-on event velocity parameter
    gkMod_$CHA     = 0     ; cc1 Modulation
    ;kData          = 2     ; cc6 Parameter-data
    ;kDummy         = 2     ; throw away cc101
    gkBenRan_$CHA  = 2     ; pitch bend range in semitones
    gkVol_$CHA     = 0.93  ; cc7 Channel-volume
    giPatch_$CHA   = 1
    gkPan_$CHA     = 0.5   ; cc10 Pan
    gkExp_$CHA     = 0.93  ; cc11 Expression
    giAttack_$CHA  = 13    ; cc73
    giDecay_$CHA   = 1     ; cc75
    giRelease_$CHA = 0.03  ; cc72
    gkVibRat_$CHA  = 6     ; cc76 4 to 8 Hz
    gkVibDep_$CHA  = 0     ; cc77 0 to 1
    giVibDel_$CHA  = 1     ; cc78 0.1 to 10 sec logarithmic
    gkOvrDrv_$CHA  = 0     ; Overdrive Distortion   NON-STANDARD cc87
    gkRinMod_$CHA init 127 ; Ring Modulator Channel NON-STANDARD cc88
    ; prints "Init gkRinMod_$CHA=%g%n", gkRinMod_$CHA
    gkTreRat_$CHA  = 5     ; Hz NON-STANDARD cc90
    gkTreDep_$CHA  = 0     ; cc92
    gkBend_$CHA    = 0
    kBend          = 1
    gaSig_$CHA     = 0     ; holds the dry signal so reverb can be added later
    p4             = 0     ; get rid of it, it can be persistent :-)

  else
    midinoteonoct iOct, iAmp
    ; file:///home/pjb/html/csound_manual/html/cpsoct.html
    ; cpsoct — Converts an octave-point-decimal value to cycles-per-second
    iFre           = cpsoct(iOct) ; / 256   ; 20180311
    iAmp = iAmp/127
    ; prints "iOct=%g iFre=%g iAmp=%g%n", iOct, iFre, iAmp

    $GetController(7'gkVol_$CHA'0'1)  ; Channel-volume, re-ranged 0 to 1
    $GetController(11'gkExp_$CHA'0'1) ; Expression
    kVol      = gkVol_$CHA * gkExp_$CHA
    aVol      interp  kVol ; create a smooth a-rate kVol

    midiprogramchange giPatch_$CHA
    ; stubbornly resets giPatch_$CHA=-1 before a ProgramChange has occurred
    if giPatch_$CHA < 0 then
      giPatch_$CHA = 0   ; Patch 0 is the default
    endif
    ; prints "giPatch_$CHA=%g%n", giPatch_$CHA ; maudio 1st P=2 seems to fail...
    ; this prints the patch correctly every note

    ; Modulation
    $GetController(1'gkMod_$CHA'0'1)
    ; tabmorph just gets one indexed element :-(
    ; ftmorf creates a table, but the morphed fts must be numbered not named
    ; (because the table-numbers must be stored in a table).
    kPatch init giPatch_$CHA
    if kPatch < 8 then
      ftmorf    giPatch_$CHA+giPatch_$CHA+gkMod_$CHA, 99, $CHA
    endif

    ; Pan
    $GetController(10'gkPan_$CHA'0'1)
    gkLPan      = sqrt(1.0-gkPan_$CHA)
    gkRPan      = sqrt(gkPan_$CHA)

    ; The normal MIDI-Envelope
    midicontrolchange  73, giAttack_$CHA, 0.01, 1
    iAttack            = 10 * giAttack_$CHA * giAttack_$CHA
    midicontrolchange  75, giDecay_$CHA,  0.001, 1
    iDecay             = 2 * giDecay_$CHA
    midicontrolchange  72, giRelease_$CHA, 0.001, 1
    iRelease           = 10 * giRelease_$CHA
    ; prints "iAttack=%g iDecay=%g iRelease=%g%n", iAttack,iDecay,iRelease
    if giDecay_$CHA > 0.99 then   ; sustained...
      kEnv     linsegr  0, iAttack,1, iRelease,0
    else
      kEnv     linsegr  0, iAttack,1, iDecay,0.707, iDecay,0.5, iDecay,0.353, \
               iDecay,0.250, iDecay,0.177, iDecay,0.125, iDecay,0.088, \
               iDecay,0.062, iDecay,0.044, iDecay,0.031, iDecay,0.022, \
               iDecay,0.015, iDecay,0.011, iDecay,0.007, iRelease,0
    endif
    aEnv     interp   kEnv ; create a smoothed a-rate envelope

    ; Vibrato
    $GetController(76'gkVibRat_$CHA'4'8)  ; Hz
    $GetController(77'gkVibDep_$CHA'0'1)  ; max nearly 2 semitones
    midicontrolchange  78, giVibDel_$CHA, 0, 1
    iVibDel     =     0.1 * 100^giVibDel_$CHA   ; sec
    kVibEnv     linseg  0, 0.7*iVibDel, 0, 0.6*iVibDel, 0.1, 1, 0.1
    kVib        oscil   gkVibDep_$CHA*gkVibDep_$CHA, gkVibRat_$CHA, 11

    ; Overdrive Distortion   NON-STANDARD
    $GetController(87'gkOvrDrv_$CHA'0.9'4)   ; off, plus 1..4

    ; Ring Modulation   NON-STANDARD
    midicontrolchange  88, gkRinMod_$CHA
    gkRinMod_$CHA = int(0.5 + gkRinMod_$CHA)

    ; Tremolo (use cc90 to mean Tremolo Rate)
    $GetController(90'gkTreRat_$CHA'2'8)  ; Hz NON-STANDARD
    $GetController(92'gkTreDep_$CHA'0'1)
    kTre        oscil   gkTreDep_$CHA, gkTreRat_$CHA, 11
    kTre        = kTre + 1

    ; Pitch Bend
    ; midicontrolchange   6, kData           ; Data
    ; gkBenRan_$CHA = kData
    ; $GetPitchBend(kBend)
    $GetPitchBend(gkBenRan_$CHA)
    ; kBend       = (round(gkBenRan_$CHA) * kBend) / 12
    ; kBend       = (round(2) * kBend) / 12
    ; gkBend_$CHA portk   kBend, kPortTime ; smoothed using portk

    aAmp        init iAmp         ; a-rate otherwise the product is wrong ?!
    aAmp2       = aAmp*aVol*aEnv*kTre
    ; prints "aAmp=%g aVol=%g aEnv=%g kTre=%g%n", aAmp, aVol, aEnv, kTre
    ; kFre        = iFre * kBend * (1.0 + kVibEnv*kVib)  kBend too small range
    kFre        = iFre * (1.0 + gkBenRan_$CHA*0.12) * (1.0 + kVibEnv*kVib)
    ; prints "iFre=%g kBend=%g kFre=%g%n", iFre, kBend, kFre
    if giPatch_$CHA == 0 then     ; Sine modulating to Triangle
      ; ares oscil xamp, xcps, ifn [, iphs]
      ; file:///home/pjb/html/csound_manual/html/oscil.html
      aSig      oscil   0.98*aAmp2, kFre, $CHA ; play the morfed table
    elseif giPatch_$CHA == 1 then ; Triangle modulating to Spiky triangle
      aSig      oscil   0.48*aAmp2, kFre, $CHA
    elseif giPatch_$CHA == 2 then ; 65% Square modulating to 100% Square
      aSig      oscil   0.44*aAmp2, kFre, $CHA
    elseif giPatch_$CHA == 3 then ; Square modulating to Sawtooth
      aSig      oscil   0.60*aAmp2, kFre, $CHA
    elseif giPatch_$CHA == 4 then ; Sawtooth modulating to Fractalised Sawtooth
      aSig      oscil   0.44*aAmp2, kFre, $CHA
    elseif giPatch_$CHA == 5 then ; 75% Sawtooth modulating to Fractalised 75%
      aSig      oscil   0.60*aAmp2, kFre, $CHA
    elseif giPatch_$CHA == 6 then ; Sine modulating to 9 harmonics of a Square
      aSig      oscil   0.90*aAmp2, kFre, $CHA
    elseif giPatch_$CHA == 7 then ; Fat Sine modulating to Thin Sine
      aSig      oscil   0.56*aAmp2, kFre, $CHA
    elseif giPatch_$CHA == 8 then ; Buzz
      aSig      gbuzz   0.4*aAmp2, kFre, 70, 0, 0.45+0.4*gkMod_$CHA, 11
    elseif giPatch_$CHA == 9 then ; Sawtooth modulating to Square
      aSig      oscil   0.54*aAmp2, kFre, $CHA
    elseif giPatch_$CHA == 10 then ; RectSin modulating to more harmonics
      aSig      oscil   0.64*aAmp2, kFre, $CHA
    elseif giPatch_$CHA == 11 then
      aSig1     oscil   0.64*aAmp2, kFre, 11
      aSig2     oscil   0.64*aAmp2, 2.0*kFre, 11
      aSig4     oscil   0.64*aAmp2, 4.0*kFre, 11
      aSig      = 0.80*aSig1 + 0.10*aSig2 + 0.20*aSig4
    elseif giPatch_$CHA == 12 then
      kFre        = cpsoct(kBend) * (1.0 + kVibEnv*kVib)
      aSig      pinkish 1
      aSig      lowpass2  aSig, kFre, 6
      aSig      = 0.70*aAmp2*aSig
    elseif giPatch_$CHA == 13 then
      aSig      oscili   0.68*aAmp2, kFre, giThinSin3
    elseif giPatch_$CHA == 14 then
      aSig      oscili   0.60*aAmp2, kFre, giThinSin5
    elseif giPatch_$CHA == 15 then
      aSig      oscili   0.64*aAmp2, kFre, giRectSin
    elseif giPatch_$CHA == 91 then
      aL, aR    ins
      aSig      = 0.5 * (aL+aR)
    else  ; default
      aSig      oscil    0.15*aAmp2, kFre, giTria
    endif
    gaSig_$CHA  = gaSig_$CHA + aSig ; Channel total. The rest is in PostProcess
  endif
#

instr 1    ; channel 0
  $AudioChannel(1)
endin
instr 2    ; channel 1
  $AudioChannel(2)
endin
instr 3    ; channel 2
  $AudioChannel(3)
endin
instr 4    ; channel 3
  $AudioChannel(4)
endin
instr 5    ; channel 4
  $AudioChannel(5)
endin
instr 6    ; channel 5
  $AudioChannel(6)
endin
instr 7    ; channel 6
  $AudioChannel(7)
endin
instr 8    ; channel 7
  $AudioChannel(8)
endin
instr 9    ; channel 8
  $AudioChannel(9)
endin

instr 10    ; channel 9 is the Percussion, as in GM
  kkey      init 0
  kvelocity init 0
  midinoteonkey kkey, kvelocity
  kAmp      = kvelocity / 127

  kcontroller      init 0
  kcontrollervalue init 0
  midicontrolchange kcontroller, kcontrollervalue
  kVol      init 0.8
  kLPan     = 0.707
  kRPan     = 0.707
  if kcontroller == 7 then
    kVol    = kcontrollervalue / 127
  elseif kcontroller == 10 then
    kPan    = kcontrollervalue / 127
  endif
  kTrigger    changed  kPan ; if kPan changes, recalculate kLPan, kRPan
  if kTrigger == 1 then
    kLPan   = sqrt(1.0-kPan)
    kRPan   = sqrt(kPan)
  endif
  ; prints "kLPan=%g kRPan=%g%n", kPan, kLPan, kRPan

  if kkey == 33 then ; metronome beat
    kEnv    expseg .6, 0.04, .02, 0.2, .000005
    aSig    oscili  kAmp*kVol*kEnv, 1100, 11
  elseif kkey == 34 then ; metronome barline
    kEnv    expseg .4, 0.15, .02, 0.6, .000005
    aSig    oscili  kAmp*kVol*kEnv, 3000, 11
  elseif kkey == 35 then ; Acoustic Bass Drum
    kEnv    expseg 1, 0.1, .15, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 50, 11
  elseif kkey == 36 then ; Bass Drum 1
    kEnv    expseg 1, 0.1, .2, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 65, 11
  elseif kkey == 37 then ; Side Stick
    kEnv    expseg 1.1, 0.05, .001, 0.5, .00001
    aSig    oscili  kAmp*kVol*kEnv, 900, giTria
  elseif kkey == 38 then ; Acoustic Snare
    kEnv    expseg 1, 0.1, .2, 0.2, .002
    aSig    noise  kAmp*kVol*kEnv, 0.0
  elseif kkey == 40 then ; Electric Snare
    kEnv    expseg 1, 0.1, .2, 1, .002
    aSig    noise  kAmp*kVol*kEnv, 0.7
  elseif kkey == 41 then ; Tom
    kEnv    expseg 1, 0.1, .1, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 105, 11
  elseif kkey == 43 then ; Tom
    kEnv    expseg 1, 0.1, .1, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 122, 11
  elseif kkey == 45 then ; Tom
    kEnv    expseg 1, 0.1, .1, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 142, 11
  elseif kkey == 48 then ; Tom
    kEnv    expseg 1, 0.1, .1, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 165, 11
  elseif kkey == 50 then ; Tom
    kEnv    expseg 1, 0.1, .1, 1, .002
    aSig    oscili  kAmp*kVol*kEnv, 190, 11
  elseif kkey == 60 then ; High Bongo
    kEnv    expseg .5, 0.1, .02, 1, .000005
    aSig    oscili  kAmp*kVol*kEnv, 410, 11
  elseif kkey == 61 then ; Low Bongo
    kEnv    expseg .5, 0.1, .05, 1, .00005
    aSig    oscili  kAmp*kVol*kEnv, 300, 11
  elseif kkey == 62 then ; Mute High Conga
    kEnv    expseg 1, 0.1, .002, 0.5, .00001
    aSig    oscili  kAmp*kVol*kEnv, 215, 11
  elseif kkey == 63 then ; Open High Conga
    kEnv    expseg 1, 0.1, .10, 1, .00001
    aSig    oscili  kAmp*kVol*kEnv, 205, 11
  elseif kkey == 64 then ; Low Conga
    kEnv    expseg 1, 0.1, .12, 1, .0001
    aSig    oscili  kAmp*kVol*kEnv, 150, 11
  elseif kkey == 81 then ; Open Triangle
    kEnv    expsegr 0.3, 0.01, .15, 10, .002, 10, .0001
    aSig    oscili  kAmp*kVol*kEnv, 4000, 11
  endif
  outs      kLPan*aSig, kRPan*aSig
endin

instr 11   ; channel 10
  $AudioChannel(11)
endin
instr 12   ; channel 11
  $AudioChannel(12)
endin
instr 13   ; channel 12
  $AudioChannel(13)
endin
instr 14   ; channel 13
  $AudioChannel(14)
endin
instr 15   ; channel 14
  $AudioChannel(15)
endin
instr 16   ; channel 15
  $AudioChannel(16)
endin

#define PostProcess(CHA) #

  ; Ring Modulator must be here so all channels have set their gaSig_* already
  if gkRinMod_$CHA < 8.5 then
    if gkRinMod_$CHA < 0.5 then ; Ring-Modulate with Channel 0
      gaSig_$CHA = gaSig_$CHA * gaSig_1
      gaSig_1 = 0
    elseif gkRinMod_$CHA < 1.5 then ; with Channel 1
      gaSig_$CHA = gaSig_$CHA * gaSig_2
      gaSig_2 = 0
    elseif gkRinMod_$CHA < 2.5 then
      gaSig_$CHA = gaSig_$CHA * gaSig_3
      gaSig_3 = 0
    elseif gkRinMod_$CHA < 3.5 then
      gaSig_$CHA = gaSig_$CHA * gaSig_4
      gaSig_4 = 0
    elseif gkRinMod_$CHA < 4.5 then
      gaSig_$CHA = gaSig_$CHA * gaSig_5
      gaSig_5 = 0
    elseif gkRinMod_$CHA == 5 then
      gaSig_$CHA = gaSig_$CHA * gaSig_6
      gaSig_6 = 0
    elseif gkRinMod_$CHA < 6.5 then
      gaSig_$CHA = gaSig_$CHA * gaSig_7
      gaSig_7 = 0
    elseif gkRinMod_$CHA < 7.5 then
      gaSig_$CHA = gaSig_$CHA * gaSig_8
      gaSig_8 = 0
    else
      gaSig_$CHA = gaSig_$CHA * gaSig_9
      gaSig_9 = 0
    endif
    gaSig_$CHA = gaSig_$CHA * 2
  endif

  ; gkOvrDrv_$CHA
  ; /home/ports/swh-plugins-0.4.15/foverdrive_1196.c
  ; drivem1 = drive - 1.0 ; float fx = fabs(x);
  ; output   x*(fx + drive)/(x*x + drivem1*fx + 1.0);
  if gkOvrDrv_$CHA > 0.95 then
    kDm1 = gkOvrDrv_$CHA - 1
    aAbs = abs(gaSig_$CHA)
    gaSig_$CHA = gaSig_$CHA * ( aAbs + gkOvrDrv_$CHA ) \
      / ( gaSig_$CHA*gaSig_$CHA + kDm1*aAbs + 1.0 )
  endif

  ; become stereo
  aSigL = 0.25 * gkLPan * gaSig_$CHA
  aSigR = 0.25 * gkRPan * gaSig_$CHA

  outs     aSigL, aSigR
  gaSig_$CHA   = 0       ; empty the receiver for the next pass
#

instr 99   ; (highest instr number executed last, see reverb.csd)
  $PostProcess(1)
  $PostProcess(2)
  $PostProcess(3)
  $PostProcess(4)
  $PostProcess(5)
  $PostProcess(6)
  $PostProcess(7)
  $PostProcess(8)
  $PostProcess(9)
endin

</CsInstruments>
<CsScore>

; Table space is allocated in primary memory, along with instrument
; data space. The maximum table number used to be 200. This has been
; changed to be limited by memory only. (Currently there is an internal
; soft limit of 300, this is automatically extended as required.)
f 1 0 1024 10 1 ; morphed table for instr 1 (channel 0)
f 2 0 1024 10 1 ; morphed table for instr 2
f 3 0 1024 10 1 ; morphed table for instr 3
f 4 0 1024 10 1 ; morphed table for instr 4
f 5 0 1024 10 1 ; morphed table for instr 5
f 6 0 1024 10 1 ; morphed table for instr 6
f 7 0 1024 10 1 ; morphed table for instr 7
f 8 0 1024 10 1 ; morphed table for instr 8
f 9 0 1024 10 1 ; morphed table for instr 9 (channel 8)
f 11 0 1024 10 1   ; Sine wave
f 12 0 1024  7 0 256 1  512 -1 256  0 ; Triangle wave
f 13 0 1024  7 0  90 1  332  1 180 -1 332 -1 90 0  ; 65% Square wave
f 14 0 1024  7 0  26 1  460  1  52 -1 460 -1 26 0  ; 90% Square wave
f 15 0 1024  7 0   1 1  510  1   2 -1 510 -1  1 0  ; 100% Square wave
f 16 0 1024  7 0  86 0  170  1 170 0 172 0 170 -1 170 0 86 0 ; Spiky Triangle
f 17 0 1024  7 0 \
 1  1  56  1  1  0.5  56  0.5  1  1  56  1 \
 1  0  56  0  1  0.5  55  0.5  1  0  56  0 \
 1  1  56  1  1  0.5  56  0.5  1  1  56  1 \
 1 -1  56 -1  1 -0.5  56 -0.5  1 -1  56 -1 \
 1  0  56  0  1 -0.5  55 -0.5  1  0  56  0 \
 1 -1  56 -1  1 -0.5  56 -0.5  1 -1  56 -1         ; Castellated square
f 18 0 1024  7 0  1 1  1022 -1  1 0 ; Sawtooth
; Fractalised Sawtooth
f 19 0 1024  7 0  1 1  245 0  1 0.5  255 -0.5 1 0.5 255 -0.5 1 0 254 -1  1 0
f 20 0 1024  7 0  64 1  896 -1  64 0 ; 75% Sawtooth
; Fractalised 75% Sawtooth
f 21 0 1024  7 0  64 1  192 0 32 0.5 192 -0.5 64 0.5 192 -0.5 32 0 192 -1 64 0
f 22 0 1024 10 0.75 0 0.25 0 .15 0 .11 0 .08 ; 9 harmonics of a Square
f 23 0 1024 8 0  64 .6186  64 .8409  64 .9612 \
 64 1  64 .9612  64 .8409  64 .6186  64 0  64 -.6186  64 -.8409  \
 64 -.9612  64 -1  64 -.9612  64 -.8409  64 -.6186  64 0 ; Fat (sqrt of) sine
;giThinSin3 ftgen    0, 0, 1024, 10, 0.75, 0, -0.25
f 24 0 1024 10 0.6522 0 -0.2174 0 .1304 ; Thin Sine
f 25 0 1024 8 -1 64 -0.6936  64 -0.3989  64 -0.1273  \
 64 .1107  64 .3061  64 .4512  64 .5406  64 .5708  64 .5406  64 .4512  \
 64 .3061  64 .1107  64 -.1273  64 -.3989  64 -.6936  64 -1 ; RectSine
f 26 0 1024 7 \
-1 16 -0.832808 16 -0.672529 16 -0.525566 16 \
-0.397343 16 -0.291916 16 -0.211696 16 -0.157307 16 \
-0.127581 16 -0.11971 16 -0.129512 16 -0.109437 16 \
-0.0154567 16 0.0632491 16 0.12264 16 0.159844 16 \
0.17344 16 0.219845 16 0.242497 16 0.242674 16 \
0.223103 16 0.187683 16 0.225454 16 0.292245 16 \
0.340371 16 0.365515 16 0.364736 16 0.336738 16 \
0.282018 16 0.202866 16 0.103218 16 -0.0116137 16 \
-0.135337 16 -0.0116137 16 0.103218 16 0.202866 16 \
0.282018 16 0.336738 16 0.364736 16 0.365515 16 \
0.340371 16 0.292245 16 0.225454 16 0.187683 16 \
0.223103 16 0.242674 16 0.242497 16 0.219845 16 \
0.17344 16 0.159844 16 0.12264 16 0.0632491 16 \
-0.0154567 16 -0.109437 16 -0.129512 16 -0.11971 16 \
-0.127581 16 -0.157307 16 -0.211696 16 -0.291916 16 \
-0.397343 16 -0.525566 16 -0.672529 16 -0.832808 16 -1 ; RectSine + harmonics

; f99 is the morphing partners, 11<->12, 12<->16  etc
f 99 0   32 -2  11 12  12 16  13 15  15 17  18 19  20 21  11 22  23 24 \
                11 12  18 15  25 26  11 12  11 12  11 12  11 12  11 12
; giSqua65   ftgen    0, 0, 1024, 7, 0, 90, 1, 332, 1, 180, -1, 332, -1, 90, 0
; giSqua90   ftgen    0, 0, 1024, 7, 0, 26, 1, 460, 1,  52, -1, 460, -1, 26, 0
; giSqua     ftgen    0, 0, 1024, 7, 0,  1, 1, 510, 1,   2, -1, 510, -1,  1, 0

i 1 0 0 1 ; inititialise the instruments
i 2 0 0 1
i 3 0 0 1
i 4 0 0 1
i 5 0 0 1
i 6 0 0 1
i 7 0 0 1
i 8 0 0 1
i 9 0 0 1
i 11 0 0 1
i 12 0 0 1
i 13 0 0 1
i 14 0 0 1
i 15 0 0 1
i 16 0 0 1

i 99 0 3600  ; keep the add-reverb-and-output instrument running

f 0 3600

e
</CsScore>
</CsoundSynthesizer>

/*

my $Version       = '1.22';
my $VersionDate   = '14jan2013';

=pod

=head1 NAME

pj3synth.csd - a Csound script for a low-tech old-fashioned synth

=head1 SYNOPSIS

 amidi -l               # to get a list of your MIDI-devices (on linux)
 csound -Mhw:1,0 pj3synth.csd          # connect from MIDI-device hw:1,0
 csound -O null -Mhw:1,0 pj3synth.csd  # doesn't log to the screen
 csound -F in.mid pj3synth.csd         # plays the MIDI-file in.mid
 csound -i in.wav pj3synth.csd         # in.wav will appear as patch 91

 csound -O null pj3synth.csd  # doesn't log to the screen
 csound -T -F in.mid pj3synth.csd   # plays the MIDI-file in.mid
 csound -i in.wav pj3synth.csd  # in.wav will appear as patch 91
 perldoc pj3synth.csd           # read the manual

=head1 DESCRIPTION

This I<csound> script takes MIDI input and produces audio output.
It is still at an early release stage, and is likely to change
even in important details like Patches and Controller-numbers.

It is a version of the old pjbsynth.csd, stripped down to save CPU load.
It does without Phaser, Reverb, Registered Parameters, and Data Entry.
The Pitch-wheel range is therefore fixed at the default -2 to +2 semitones.
It also does without the "fractal patches".

On linux, the MIDI-device must be a virtual-midi device,
which you may need to start from your C</etc/rc.local> file by

    modprobe snd-virmidi snd_index=1

Channels 0-8 are dedicated to Audio signals.
The basic Patches are simple, old-fashioned waveforms
like sine, triangle, square, and sawtooth,
plus some fractal waveforms, and also live or file audio.
You will probably want to use Controller 75 to set the Decay-time,
and Controller 73 for the Attack-time.

The Channel Controllers for these Audio channels
can be set by the normal methods,
or one or two of them can be driven by a built-in Low-Frequency-Oscillator
by using the non-standard controllers cc20 to cc23, or cc24 to cc27,
or by a built-in Attack-Envelope using cc52 to cc53.
Other non-standard controllers offer also
Ring modulation, Distortion, some unusual types of Loop, etc.

Channel 9 is dedicated to a low-tech percussion-set
using simple waveforms like sine or white noise.
It is roughly modelled on General-MIDI Channel 9,
see http://www.pjb.com.au/muscript/gm.html#perc

Channels 10-15 are dedicated to the Low-Frequency-Oscillators,
where MIDI-note number 60 means not middle-C,
but 1 Cycle-per-second (i.e. eight octaves lower!).

An example I<setup1.mid> is included,
which sets up I<pj3synth> with a few plausible sounds.

=head1 PATCHES

=over 3

=item B<Audio Patches> for channels 0-8 (Note-number 60 is middle-C)

  0 = Sine wave       (modulating to triangle)
  1 = Triangle wave   (modulating to spiky triangle)
  2 = 65% Square wave (modulating to 100% square wave)
  3 = Square wave     (modulating to castellated-square wave)
  4 = Sawtooth wave   (modulating to fractalised sawtooth)
  5 = 75%-Sawtooth    (modulating to fractalised 75% sawtooth)
  6 = Sine wave       (modulating to 9 harmonics of a square wave)
  7 = Fat Sine wave   (modulating to a 5-harmonic thin sine wave)
  8 = Buzz            (round modulating to buzzy)
  9 = Sawtooth wave   (modulating to square wave)
 10 = Rectified Sine  (modulating to rectified sine with harmonics)
 12 = Fat Sine
 13 = Thin Sine with 3rd harmonic
 14 = Thin Sine with 3rd and 5th harmonics
 15 = Rectified Sine
 90 = Live stereo audio input (currently unimplemented)
 91 = Live audio input mixed down to mono

=item B<LFO Patches> for channels 10-15 (Middle-C means 1 cycle/second !)

The Low-Frequency-Oscillators live in Channels 10 and above,
and use Patches 100 and above.
Their frequencies are eight octaves lower than the audio Patches,
so that Note number 60 means 1 cycle-per-second.
These two data-items, the Patch and the "Note",
are the only two that Low-Frequency-Oscillators need.

 100 = Sine wave
 101 = Triangle wave
 102 = Square wave 65%
 103 = Square wave 90%
 104 = Square wave
 105 = Sawtooth wave Up
 106 = Intermediate Triangle/Sawtooth wave 90% Up
 107 = Intermediate Triangle/Sawtooth wave 75% Up
 108 = another Triangle wave
 109 = Intermediate Triangle/Sawtooth wave 75% Down
 110 = Intermediate Triangle/Sawtooth wave 90% Down
 111 = Sawtooth wave Down

=back

=head1 STANDARD CONTROLLER-NUMBERS

=over 3

See http://www.pjb.com.au/muscript/gm.html#cc

   1 = Modulation
   7 = Channel volume
  10 = Pan
  11 = Expression
  64 = Sustain pedal
  72 = Release time
  73 = Attack time
  75 = Decay time
  76 = Vibrato rate
  77 = Vibrato depth
  78 = Vibrato delay
  92 = Tremolo depth

=back

=head1 NON-STANDARD CONTROLLER-NUMBERS

cc20-23 connect a controller (cc21) to an LFO (cc20),
and cc24-27 connect another controller (cc25) to an LFO (cc24).
Similarly, cc52-55 connect a controller (cc53)
to an Effect-Envelope launched at the start of each note
(which also affects other notes ongoing in the same Channel).

When being driven by an LFO or an Effect-Envelope in this way,
the controller will ignore its standard MIDI Controller-Change commands.

If in one channel cc21 and cc25 both attempt to connect the same controller
(this would be an error) then the cc20-cc23 specification takes precedence.
But multiple controllers in many channels may be driven by the same LFO
without problem.

 20 = LFO-channel (10..15; default 0=off)
 21 = an audio Channel-Controller that the LFO will control
      (cc21=0 is special-cased to mean Pitch-Bend)
 22 = the minimum value of that audio Channel-Controller
 23 = the maximum value of that audio Channel-Controller

 24 = LFO-channel (10..15; default 0=off)
 25 = another audio Channel-Controller that this new LFO will control
      (cc25=0 is special-cased to mean Pitch-Bend)
 26 = the minimum value of that audio Channel-Controller
 27 = the maximum value of that audio Channel-Controller

 52 = Effect-Envelope Attack-Time (0..127; default 0=off)
 53 = the audio Channel-Controller that this Effect-Envelope will control
      (cc53=0 is special-cased to mean Pitch-Bend)
 54 = the initial value of that audio Channel-Controller
 55 = the  final  value of that audio Channel-Controller

There are several other non-standard CC's:

  14 = Choof (unimplemented)
  15 = Clonk (unimplemented)
  87 = Overdrive Distortion (modelled on SWH foverdrive_1196)
  88 = Ring Modulator Channel (usually set to Patch 0)
  90 = Tremolo rate (affects the cc92 tremolo)

If cc87=0 then Distortion is switched off.

If cc88=127 then Ring-Modulation is switched off.
If cc88 points to another audio channel (0..8) then
the latter's direct output will be suppressed
and the current channel will be modulated with it.
Usually, that other channel should be a simple wave-form like a sine,
perhaps with portamento.

=head1 INSTALLATION

For a full installation, download the tarball:
http://www.pjb.com.au/midi/free/pjbsynth-1.22.tar.gz ,
unpack it, and, as superuser: I<make install> .
This puts some files in I</usr/local/share/pjbsynth/>
and copies a wrapper script to I</usr/local/bin/pjbsynth>

For a minimal installation, unpack the tarball and
copy I<pjbsynth.csd> into your working directory.

See the SYNOPSIS for the difference in usage.

You will also need I<csound> version 5 installed.

=head1 CHANGES

 20180311 1.23 stripped down for performance, and several bugs fixed
 20130117 1.22 fix two bugs reported by Pete Goodeve on csound 5.19
 20110120 1.21 Loop stuff moved to CCs 44-74; Rabbit and MT2 Loops
 20110104 1.20 standard cc71 means filter Q :-) so get rid of cc89
 20101228 1.19 cc85 and cc86 now do Loop, not just Echo
 20101224 1.18 two more modulating patches
 20101220 1.17 cc11 Expression
 20101219 1.16 tarball includes the up-to-date test_score
 20101219 1.15 fixed cc53=0 Envelope-driven Bend bug; other minor fixes
 20101217 1.14 fixed LFO bug, new modulating patches 0..6
 20101211      disentangled cc53=0 (Bend) from the other cc53 settings
 20101210 1.13 stereo phaser, cc52-55 allow attack-Envelope control
 20101202 1.12 Ring modulator, Distortion, Patch 91
 20101127 1.11 first uploaded to www.pjb.com.au
 20101110 1.10 first working version

 TODO:
  Choof CC using perhaps very mildly filtered pink noise
  Clonk CC using very heavily damped sine at perhaps sqrt of note-pitch
  Note=0 guaranteed silent (used to force a reread of Loop parameters)
  A filtered pink-noise patch
  Stereo live audio, through patch 90
  State dump

=head1 BUGS

Changes in controller cc45 go unnoticed
until a new note is struck in the channel;
this is inconvenient if a loop is running and you want to fade it out...

=head1 AUTHOR

Peter J Billam   http://www.pjb.com.au/comp/contact.html

=head1 SEE ALSO

 http://www.csounds.com
 http://www.pjb.com.au
 http://www.pjb.com.au/muscript/gm.html
 http://www.pjb.com.au/midi/midikbd.html
 amidi (1)
 aconnect (1)

=cut

*/
